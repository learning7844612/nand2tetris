#include <code/code.h>
#include <exceptions/parserexception.h>
#include <fstream>
#include <iostream>
#include <lexer/lexer.h>
#include <lexer/token.h>
#include <parser/parser.h>
#include <string>

Parser::Parser(std::string fileName) : inFile(std::ifstream(fileName)), lineNumber(1), currentCommandType(I_COMMAND)
{
    if (inFile.fail())
    {
        throw ParserException("Error while opening the file " + fileName + "\n");
    }
}

Parser::~Parser()
{
    inFile.close();
}

bool Parser::hasMoreCommands()
{
    return !inFile.eof();
}

void Parser::rewind()
{
    inFile.clear();
    lineNumber = 1;
    mSymbol.clear();
    mDest.clear();
    mComp.clear();
    mJump.clear();
    currentCommandType = I_COMMAND;
    inFile.seekg(0);
}

int Parser::currentLine() const
{
    return lineNumber;
}

void Parser::advance()
{
    mSymbol.clear();
    mDest.clear();
    mComp.clear();
    mJump.clear();
    currentCommandType = I_COMMAND;
    std::string line;
    std::getline(inFile, line);
    while (line.empty())
    {
        std::getline(inFile, line);
        lineNumber++;
        if (inFile.eof())
        {
            currentCommandType = I_COMMAND;
            return;
        }
    }
    Lexer lex = Lexer(line, lineNumber);
    parse(lex);
    if (inFile && inFile.peek() == EOF)
    {
        return;
    }
    lineNumber++;
}

Parser::command Parser::commandType() const
{
    return currentCommandType;
}

std::string Parser::symbol() const
{
    return mSymbol;
}

std::string Parser::dest() const
{
    return mDest;
}

std::string Parser::comp() const
{
    return mComp;
}

std::string Parser::jump() const
{
    return mJump;
}

bool Parser::label(Lexer &lex)
{
    if (currentToken.mToken == Token::RO)
    {
        currentToken = lex.getToken();
        if (currentToken.mToken == Token::ID)
        {
            mSymbol = currentToken.mValue;
            currentToken = lex.getToken();
            if (currentToken.mToken == Token::RC)
            {
                currentToken = lex.getToken();
                if (currentToken.mToken == Token::LEND)
                {
                    currentCommandType = Parser::L_COMMAND;
                    return true;
                }
                else if (currentToken.mToken == Token::COMM)
                {
                    currentToken = lex.getToken();
                    if (currentToken.mToken == Token::LEND)
                    {
                        currentCommandType = Parser::L_COMMAND;
                        return true;
                    }
                }
                else
                {
                    throw ParserException("In line " + std::to_string(lineNumber) + " column " + std::to_string(currentToken.mColumn) + ": unexpected input at the end of the line");
                }
            }
            else
            {
                throw ParserException("In line " + std::to_string(lineNumber) + " column " + std::to_string(currentToken.mColumn) + ": ')' expected");
            }
        }
        else
        {
            throw ParserException("In line " + std::to_string(lineNumber) + " column " + std::to_string(currentToken.mColumn) + ": identifier expected");
        }
    }
    return false;
}

bool Parser::aInstruction(Lexer &lex)
{
    if (currentToken.mToken == Token::AT)
    {
        currentToken = lex.getToken();
        if (currentToken.mToken == Token::CONST)
        {
            mSymbol = currentToken.mValue;
            currentToken = lex.getToken();
            if (currentToken.mToken == Token::LEND)
            {
                currentCommandType = Parser::A_COMMAND;
                return true;
            }
            else if (currentToken.mToken == Token::COMM)
            {
                currentToken = lex.getToken();
                if (currentToken.mToken == Token::LEND)
                {
                    currentCommandType = Parser::A_COMMAND;
                    return true;
                }
            }
            else
            {
                throw ParserException("In line " + std::to_string(lineNumber) + " column " + std::to_string(currentToken.mColumn) + ": unexpected input at the end of the line");
            }
        }
        else if (currentToken.mToken == Token::ID)
        {
            mSymbol = currentToken.mValue;
            currentToken = lex.getToken();
            if (currentToken.mToken == Token::LEND)
            {
                currentCommandType = Parser::A_COMMAND;
                return true;
            }
            else if (currentToken.mToken == Token::COMM)
            {
                currentToken = lex.getToken();
                if (currentToken.mToken == Token::LEND)
                {
                    currentCommandType = Parser::A_COMMAND;
                    return true;
                }
            }
            else
            {
                throw ParserException("In line " + std::to_string(lineNumber) + " column " + std::to_string(currentToken.mColumn) + ": unexpected input at the end of the line");
            }
        }
        else
        {
            throw ParserException("In line " + std::to_string(lineNumber) + " column " + std::to_string(currentToken.mColumn) + ": identifier or constant expected");
        }
    }
    return false;
}

bool Parser::cInstruction(Lexer &lex)
{
    if (currentToken.mToken == Token::DEST)
    {
        mDest = currentToken.mValue;
        currentToken = lex.getToken();
        if (currentToken.mToken == Token::EQ)
        {
            currentToken = lex.getToken();
            if (currentToken.mToken == Token::COMP)
            {
                mComp = currentToken.mValue;
                currentToken = lex.getToken();
                if (currentToken.mToken == Token::SC)
                {
                    currentToken = lex.getToken();
                    if (currentToken.mToken == Token::JUMP)
                    {
                        mJump = currentToken.mValue;
                        currentToken = lex.getToken();
                        if (currentToken.mToken == Token::LEND)
                        {
                            currentCommandType = Parser::C_COMMAND;
                            return true;
                        }
                        else if (currentToken.mToken == Token::COMM)
                        {
                            currentToken = lex.getToken();
                            if (currentToken.mToken == Token::LEND)
                            {
                                currentCommandType = Parser::C_COMMAND;
                                return true;
                            }
                        }
                        else
                        {
                            throw ParserException("In line " + std::to_string(lineNumber) + " column " + std::to_string(currentToken.mColumn) + ": unexpected input at the end of the line");
                        }
                    }
                    else
                    {
                        throw ParserException("In line " + std::to_string(lineNumber) + " column " + std::to_string(currentToken.mColumn) + ": jump expression expected");
                    }
                }
                else if (currentToken.mToken == Token::LEND)
                {
                    currentCommandType = Parser::C_COMMAND;
                    return true;
                }
                else if (currentToken.mToken == Token::COMM)
                {
                    currentToken = lex.getToken();
                    if (currentToken.mToken == Token::LEND)
                    {
                        currentCommandType = Parser::C_COMMAND;
                        return true;
                    }
                }
                else
                {
                    throw ParserException("In line " + std::to_string(lineNumber) + " column " + std::to_string(currentToken.mColumn) + ": unexpected input at the end of the line");
                }
            }
            else
            {
                throw ParserException("In line " + std::to_string(lineNumber) + " column " + std::to_string(currentToken.mColumn) + ": comp expression expected");
            }
        }
        else
        {
            throw ParserException("In line " + std::to_string(lineNumber) + " column " + std::to_string(currentToken.mColumn) + ": '=' expected");
        }
    }
    else if (currentToken.mToken == Token::COMP)
    {
        mComp = currentToken.mValue;
        currentToken = lex.getToken();
        if (currentToken.mToken == Token::SC)
        {
            currentToken = lex.getToken();
            if (currentToken.mToken == Token::JUMP)
            {
                mJump = currentToken.mValue;
                currentToken = lex.getToken();
                if (currentToken.mToken == Token::LEND)
                {
                    currentCommandType = Parser::C_COMMAND;
                    return true;
                }
                else if (currentToken.mToken == Token::COMM)
                {
                    currentToken = lex.getToken();
                    if (currentToken.mToken == Token::LEND)
                    {
                        currentCommandType = Parser::C_COMMAND;
                        return true;
                    }
                }
                else
                {
                    throw ParserException("In line " + std::to_string(lineNumber) + " column " + std::to_string(currentToken.mColumn) + ": unexpected input at the end of the line");
                }
            }
            else
            {
                throw ParserException("In line " + std::to_string(lineNumber) + " column " + std::to_string(currentToken.mColumn) + ": jump expression expected");
            }
        }
        else if (currentToken.mToken == Token::LEND)
        {
            currentCommandType = Parser::C_COMMAND;
            return true;
        }
        else if (currentToken.mToken == Token::COMM)
        {
            currentToken = lex.getToken();
            if (currentToken.mToken == Token::LEND)
            {
                currentCommandType = Parser::C_COMMAND;
                return true;
            }
        }
        else
        {
            throw ParserException("In line " + std::to_string(lineNumber) + " column " + std::to_string(currentToken.mColumn) + ": unexpected input at the end of the line");
        }
    }
    return false;
}

void Parser::parse(Lexer &lex)
{
    currentToken = lex.getToken();
    if (currentToken.mToken == Token::COMM)
    {
        currentToken = lex.getToken();
        if (currentToken.mToken == Token::LEND)
        {
            currentToken = lex.getToken();
        }
    }
    if (label(lex) || cInstruction(lex) || aInstruction(lex) || currentToken.mToken == Token::LEND)
    {
        return;
    }
    else
    {
        throw ParserException("In line " + std::to_string(lineNumber) + " column " + std::to_string(currentToken.mColumn) + ": '(', '@', dest expression or comp expression expected");
    }
}
