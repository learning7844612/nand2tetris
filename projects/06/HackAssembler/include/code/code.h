#ifndef CODE_H
#define CODE_H

#include <string>
#include <map>

class Code
{
public:
    static std::string dest(std::string);
    static std::string comp(std::string);
    static std::string jump(std::string);

private:
    static const std::map<std::string, std::string> mDest;
    static const std::map<std::string, std::string> mComp;
    static const std::map<std::string, std::string> mJump;
};

#endif /** CODE_H */