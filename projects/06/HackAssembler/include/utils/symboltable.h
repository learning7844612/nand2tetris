#ifndef SYMBOLTABLE_H
#define SYMBOLTABLE_H

#include <map>
#include <string>

class SymbolTable
{
public:
    SymbolTable();
    void addEntry(std::string, int);
    bool contains(std::string);
    int getAddress(std::string);

private:
    std::map<std::string, int> map;
};

#endif /** SYMBOLTABLE_H */