#ifndef LEXER_H
#define LEXER_H

#include <string>
#include "token.h"

class Lexer
{
public:
    Lexer();
    Lexer(std::string, int lineNumber);
    Token getToken();

private:
    std::string mLine;
    std::string::iterator mLineIterator;
    int mLineNumber;
    int mColumn;
};

#endif /** LEXER_H */