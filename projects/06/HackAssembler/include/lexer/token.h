#ifndef TOKEN_H
#define TOKEN_H

#include <string>

class Token
{
public:
    enum token
    {
        DEST,
        COMP,
        JUMP,
        EQ,
        SC,
        RO,
        RC,
        AT,
        ID,
        CONST,
        COMM,
        INV,
        LEND
    };
    Token();
    Token(enum token, int, std::string);
    token mToken;
    int mColumn;
    std::string mValue;
};

#endif /** TOKEN_H */
