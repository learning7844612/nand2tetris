#ifndef PARSEREXCEPTION_H
#define PARSEREXCEPTION_H

#include <exception>
#include <iostream>
#include <string>

class ParserException : public std::exception
{
    std::string _msg;

public:
    ParserException(const std::string &msg) : _msg(msg) {}
    virtual const char *what() const noexcept override
    {
        return _msg.c_str();
    }
};

#endif /** PARSEREXCEPTION_H */
