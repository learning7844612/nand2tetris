#ifndef DUPLICATELABELEXCEPTION_H
#define DUPLICATELABELEXCEPTION_H

#include <exception>
#include <iostream>
#include <string>

class DuplicateLabelException : public std::exception
{
    std::string _msg;

public:
    DuplicateLabelException(const std::string &msg) : _msg(msg) {}
    virtual const char *what() const noexcept override
    {
        return _msg.c_str();
    }
};

#endif /** DUPLICATELABEREXCEPTION_h */