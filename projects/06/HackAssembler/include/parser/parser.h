#ifndef PARSER_H
#define PARSER_H

#include <fstream>
#include <string>
#include <lexer.h>
#include <token.h>

class Parser
{
public:
    Parser(std::string);
    ~Parser();
    enum command
    {
        A_COMMAND,
        C_COMMAND,
        L_COMMAND,
        I_COMMAND
    };
    bool hasMoreCommands();
    void advance();
    void rewind();
    command commandType() const;
    std::string symbol() const;
    std::string dest() const;
    std::string comp() const;
    std::string jump() const;
    int currentLine() const;

private:
    std::ifstream inFile;
    std::ofstream outFile;
    int lineNumber;
    command currentCommandType;
    std::string mDest;
    std::string mComp;
    std::string mJump;
    std::string mSymbol;
    Token currentToken;
    bool label(Lexer &);
    bool aInstruction(Lexer &);
    bool cInstruction(Lexer &);
    void parse(Lexer &);
};

#endif /** PARSER_H */
