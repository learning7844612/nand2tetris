#include <algorithm>
#include <lexer/lexer.h>
#include <string>

#define ADVANCE_AND_SAVE_TOKEN_VALUE()  \
    do                                  \
    {                                   \
        lineBuffer += *mLineIterator++; \
        mColumn++;                      \
    } while (0)

Lexer::Lexer()
{
}

Lexer::Lexer(std::string line, int lineNumber) : mLine(line), mLineNumber(lineNumber), mLineIterator(mLine.begin())
{
    mColumn = 1;
    auto iterator = mLine.begin();
    while (iterator != mLine.end())
    {
        if (*iterator++ == ' ')
        {
            mColumn++;
        }
    }
    mLine.erase(std::remove(mLine.begin(), mLine.end(), ' '), mLine.end());
    mLine.erase(std::remove(mLine.begin(), mLine.end(), '\t'), mLine.end());
    mLine.erase(std::remove(mLine.begin(), mLine.end(), '\r'), mLine.end());
}

Token Lexer::getToken()
{
    std::string lineBuffer;
    switch (*mLineIterator)
    {
    case '=':
        ADVANCE_AND_SAVE_TOKEN_VALUE();
        return Token(Token::EQ, mColumn, lineBuffer);
    case ';':
        ADVANCE_AND_SAVE_TOKEN_VALUE();
        return Token(Token::SC, mColumn, lineBuffer);
    case '(':
        ADVANCE_AND_SAVE_TOKEN_VALUE();
        return Token(Token::RO, mColumn, lineBuffer);
    case ')':
        ADVANCE_AND_SAVE_TOKEN_VALUE();
        return Token(Token::RC, mColumn, lineBuffer);
    case '@':
        ADVANCE_AND_SAVE_TOKEN_VALUE();
        return Token(Token::AT, mColumn, lineBuffer);
    }
    if (mLineIterator == mLine.end())
    {
        return Token(Token::LEND, mColumn, lineBuffer);
    }
    else if (*mLineIterator == '/' && *(mLineIterator + 1) == '/')
    {
        while (mLineIterator != mLine.end())
        {
            ADVANCE_AND_SAVE_TOKEN_VALUE();
        }
        return Token(Token::COMM, mColumn, lineBuffer);
    }
    else if (*(mLineIterator - 1) != '=' && *(mLineIterator - 1) != '(' && *(mLineIterator - 1) != '@')
    {
        if (*mLineIterator == '0' || *mLineIterator == '1')
        {
            ADVANCE_AND_SAVE_TOKEN_VALUE();
            return Token(Token::COMP, mColumn, lineBuffer);
        }
        else if (*mLineIterator == '-')
        {
            lineBuffer += *mLineIterator++;
            mColumn++;
            if (*mLineIterator == '1' || *mLineIterator == 'A' || *mLineIterator == 'D' || *mLineIterator == 'M')
            {
                ADVANCE_AND_SAVE_TOKEN_VALUE();
                return Token(Token::COMP, mColumn, lineBuffer);
            }
        }
        while (std::isalpha(*mLineIterator))
        {
            ADVANCE_AND_SAVE_TOKEN_VALUE();
        }
        if (lineBuffer == "")
        {
            if (*mLineIterator == ';')
            {
                return Token(Token::COMP, mColumn, lineBuffer);
            }
            return Token(Token::DEST, mColumn, lineBuffer);
        }
        if (lineBuffer == "M")
        {
            if (*mLineIterator == ';')
            {
                return Token(Token::COMP, mColumn, lineBuffer);
            }
            return Token(Token::DEST, mColumn, lineBuffer);
        }
        else if (lineBuffer == "D")
        {
            if (*mLineIterator == ';')
            {
                return Token(Token::COMP, mColumn, lineBuffer);
            }
            return Token(Token::DEST, mColumn, lineBuffer);
        }
        else if (lineBuffer == "MD")
        {
            if (*mLineIterator == ';')
            {
                return Token(Token::COMP, mColumn, lineBuffer);
            }
            return Token(Token::DEST, mColumn, lineBuffer);
        }
        else if (lineBuffer == "A")
        {
            if (*mLineIterator == ';')
            {
                return Token(Token::COMP, mColumn, lineBuffer);
            }
            return Token(Token::DEST, mColumn, lineBuffer);
        }
        else if (lineBuffer == "AM")
        {
            if (*mLineIterator == ';')
            {
                return Token(Token::COMP, mColumn, lineBuffer);
            }
            return Token(Token::DEST, mColumn, lineBuffer);
        }
        else if (lineBuffer == "AD")
        {
            if (*mLineIterator == ';')
            {
                return Token(Token::COMP, mColumn, lineBuffer);
            }
            return Token(Token::DEST, mColumn, lineBuffer);
        }
        else if (lineBuffer == "AMD")
        {
            if (*mLineIterator == ';')
            {
                return Token(Token::COMP, mColumn, lineBuffer);
            }
            return Token(Token::DEST, mColumn, lineBuffer);
        }
        else if (lineBuffer == "JGT")
        {
            return Token(Token::JUMP, mColumn, lineBuffer);
        }
        else if (lineBuffer == "JEQ")
        {
            return Token(Token::JUMP, mColumn, lineBuffer);
        }
        else if (lineBuffer == "JGE")
        {
            return Token(Token::JUMP, mColumn, lineBuffer);
        }
        else if (lineBuffer == "JLT")
        {
            return Token(Token::JUMP, mColumn, lineBuffer);
        }
        else if (lineBuffer == "JNE")
        {
            return Token(Token::JUMP, mColumn, lineBuffer);
        }
        else if (lineBuffer == "JLE")
        {
            return Token(Token::JUMP, mColumn, lineBuffer);
        }
        else if (lineBuffer == "JMP")
        {
            return Token(Token::JUMP, mColumn, lineBuffer);
        }
        else
        {
            return Token(Token::INV, mColumn, lineBuffer);
        }
    }
    else if (*(mLineIterator - 1) == '=')
    {
        if (*mLineIterator == '0' || *mLineIterator == '1')
        {
            ADVANCE_AND_SAVE_TOKEN_VALUE();
            return Token(Token::COMP, mColumn, lineBuffer);
        }
        else if (*mLineIterator == '-')
        {
            lineBuffer = *mLineIterator++;
            switch (*mLineIterator)
            {
            case '1':
                ADVANCE_AND_SAVE_TOKEN_VALUE();
                return Token(Token::COMP, mColumn, lineBuffer);
            case 'D':
                ADVANCE_AND_SAVE_TOKEN_VALUE();
                return Token(Token::COMP, mColumn, lineBuffer);
            case 'A':
                ADVANCE_AND_SAVE_TOKEN_VALUE();
                return Token(Token::COMP, mColumn, lineBuffer);
            case 'M':
                ADVANCE_AND_SAVE_TOKEN_VALUE();
                return Token(Token::COMP, mColumn, lineBuffer);
            default:
                return Token(Token::INV, mColumn, lineBuffer);
            }
        }
        else if (*mLineIterator == '!')
        {
            ADVANCE_AND_SAVE_TOKEN_VALUE();
            switch (*mLineIterator)
            {
            case 'D':
                ADVANCE_AND_SAVE_TOKEN_VALUE();
                return Token(Token::COMP, mColumn, lineBuffer);
            case 'A':
                ADVANCE_AND_SAVE_TOKEN_VALUE();
                return Token(Token::COMP, mColumn, lineBuffer);
            case 'M':
                ADVANCE_AND_SAVE_TOKEN_VALUE();
                return Token(Token::COMP, mColumn, lineBuffer);
            default:
                return Token(Token::INV, mColumn, lineBuffer);
            }
        }
        else if (*mLineIterator == 'D')
        {
            ADVANCE_AND_SAVE_TOKEN_VALUE();
            if (*mLineIterator == '+')
            {
                ADVANCE_AND_SAVE_TOKEN_VALUE();
                switch (*mLineIterator)
                {
                case '1':
                    ADVANCE_AND_SAVE_TOKEN_VALUE();
                    return Token(Token::COMP, mColumn, lineBuffer);
                case 'A':
                    ADVANCE_AND_SAVE_TOKEN_VALUE();
                    return Token(Token::COMP, mColumn, lineBuffer);
                case 'M':
                    ADVANCE_AND_SAVE_TOKEN_VALUE();
                    return Token(Token::COMP, mColumn, lineBuffer);
                default:
                    return Token(Token::INV, mColumn, lineBuffer);
                }
            }
            else if (*mLineIterator == '-')
            {
                ADVANCE_AND_SAVE_TOKEN_VALUE();
                switch (*mLineIterator)
                {
                case '1':
                    ADVANCE_AND_SAVE_TOKEN_VALUE();
                    return Token(Token::COMP, mColumn, lineBuffer);
                case 'A':
                    ADVANCE_AND_SAVE_TOKEN_VALUE();
                    return Token(Token::COMP, mColumn, lineBuffer);
                case 'M':
                    ADVANCE_AND_SAVE_TOKEN_VALUE();
                    return Token(Token::COMP, mColumn, lineBuffer);
                default:
                    return Token(Token::INV, mColumn, lineBuffer);
                }
            }
            else if (*mLineIterator == '&')
            {
                ADVANCE_AND_SAVE_TOKEN_VALUE();
                switch (*mLineIterator)
                {
                case 'A':
                    ADVANCE_AND_SAVE_TOKEN_VALUE();
                    return Token(Token::COMP, mColumn, lineBuffer);
                case 'M':
                    ADVANCE_AND_SAVE_TOKEN_VALUE();
                    return Token(Token::COMP, mColumn, lineBuffer);
                default:
                    return Token(Token::INV, mColumn, lineBuffer);
                }
            }
            else if (*mLineIterator == '|')
            {
                ADVANCE_AND_SAVE_TOKEN_VALUE();
                switch (*mLineIterator)
                {
                case 'A':
                    ADVANCE_AND_SAVE_TOKEN_VALUE();
                    return Token(Token::COMP, mColumn, lineBuffer);
                case 'M':
                    ADVANCE_AND_SAVE_TOKEN_VALUE();
                    return Token(Token::COMP, mColumn, lineBuffer);
                default:
                    return Token(Token::INV, mColumn, lineBuffer);
                }
            }
            else
            {
                return Token(Token::COMP, mColumn, lineBuffer);
            }
        }
        else if (*mLineIterator == 'A')
        {
            ADVANCE_AND_SAVE_TOKEN_VALUE();
            if (*mLineIterator == '+')
            {
                ADVANCE_AND_SAVE_TOKEN_VALUE();
                if (*mLineIterator == '1')
                {
                    ADVANCE_AND_SAVE_TOKEN_VALUE();
                    return Token(Token::COMP, mColumn, lineBuffer);
                }
                else
                {
                    return Token(Token::COMP, mColumn, lineBuffer);
                }
            }
            else if (*mLineIterator == '-')
            {
                ADVANCE_AND_SAVE_TOKEN_VALUE();
                if (*mLineIterator == '1')
                {
                    ADVANCE_AND_SAVE_TOKEN_VALUE();
                    return Token(Token::COMP, mColumn, lineBuffer);
                }
                else if (*mLineIterator == 'D')
                {
                    ADVANCE_AND_SAVE_TOKEN_VALUE();
                    return Token(Token::COMP, mColumn, lineBuffer);
                }
                else
                {
                    return Token(Token::COMP, mColumn, lineBuffer);
                }
            }
            else
            {
                return Token(Token::COMP, mColumn, lineBuffer);
            }
        }
        else if (*mLineIterator == 'M')
        {
            ADVANCE_AND_SAVE_TOKEN_VALUE();
            if (*mLineIterator == '+')
            {
                ADVANCE_AND_SAVE_TOKEN_VALUE();
                if (*mLineIterator == '1')
                {
                    ADVANCE_AND_SAVE_TOKEN_VALUE();
                    return Token(Token::COMP, mColumn, lineBuffer);
                }
                else
                {
                    return Token(Token::COMP, mColumn, lineBuffer);
                }
            }
            else if (*mLineIterator == '-')
            {
                ADVANCE_AND_SAVE_TOKEN_VALUE();
                if (*mLineIterator == '1')
                {
                    ADVANCE_AND_SAVE_TOKEN_VALUE();
                    return Token(Token::COMP, mColumn, lineBuffer);
                }
                else if (*mLineIterator == 'D')
                {
                    ADVANCE_AND_SAVE_TOKEN_VALUE();
                    return Token(Token::COMP, mColumn, lineBuffer);
                }
                else
                {
                    return Token(Token::COMP, mColumn, lineBuffer);
                }
            }
            else
            {
                return Token(Token::COMP, mColumn, lineBuffer);
            }
        }
    }
    else if (std::isalpha(*mLineIterator) || *mLineIterator == '_' || *mLineIterator == '.' || *mLineIterator == '$' || *mLineIterator == ':')
    {
        ADVANCE_AND_SAVE_TOKEN_VALUE();
        while (std::isalnum(*mLineIterator) || *mLineIterator == '_' || *mLineIterator == '.' || *mLineIterator == '$' || *mLineIterator == ':')
        {
            ADVANCE_AND_SAVE_TOKEN_VALUE();
        }
        return Token(Token::ID, mColumn, lineBuffer);
    }
    else if (std::isdigit(*mLineIterator))
    {
        ADVANCE_AND_SAVE_TOKEN_VALUE();
        while (std::isdigit(*mLineIterator))
        {
            ADVANCE_AND_SAVE_TOKEN_VALUE();
        }
        return Token(Token::CONST, mColumn, lineBuffer);
    }
    return Token(Token::INV, mColumn, lineBuffer);
}