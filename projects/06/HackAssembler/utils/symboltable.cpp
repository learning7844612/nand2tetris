#include <iterator>
#include <map>
#include <string>
#include <utils/symboltable.h>

SymbolTable::SymbolTable()
{
    map["SP"] = 0;
    map["LCL"] = 1;
    map["ARG"] = 2;
    map["THIS"] = 3;
    map["THAT"] = 4;
    map["R0"] = 0;
    map["R1"] = 1;
    map["R2"] = 2;
    map["R3"] = 3;
    map["R4"] = 4;
    map["R5"] = 5;
    map["R6"] = 6;
    map["R7"] = 7;
    map["R8"] = 8;
    map["R9"] = 9;
    map["R10"] = 10;
    map["R11"] = 11;
    map["R12"] = 12;
    map["R13"] = 13;
    map["R14"] = 14;
    map["R15"] = 15;
    map["SCREEN"] = 16384;
    map["KBD"] = 24576;
}

void SymbolTable::addEntry(std::string key, int value)
{
    map.insert(std::make_pair(key, value));
}

bool SymbolTable::contains(std::string key)
{
    auto lookUp = map.find(key);
    if (lookUp != map.end())
    {
        return true;
    }
    return false;
}

int SymbolTable::getAddress(std::string key)
{
    auto lookUp = map.find(key);
    return lookUp->second;
}
