#!/bin/sh

if test -z "$1"; then
    echo "Usage: $0 --setup | --compile | --clean-setup | --clean-compile"
else
    if [ $1 == "--setup" ]; then
        mkdir -p CMake
        mkdir -p bin
        cd CMake
        cmake ..
    elif [ $1 == "--compile" ]; then
        cd CMake
        make
        mv ./HackAssembler ../bin
    elif [ $1 == "--clean-setup" ]; then
        rm -rf CMake
        rm -rf bin
    elif [ $1 == "--clean-compile" ]; then
        cd CMake
        make clean
    else
        echo "Usage: $0 --setup | --compile | --clean-setup | --clean-compile"
    fi
fi