#include <algorithm>
#include <bitset>
#include <code/code.h>
#include <exception>
#include <exceptions/duplicatelabelexception.h>
#include <exceptions/outofmemoryexception.h>
#include <exceptions/parserexception.h>
#include <iostream>
#include <filesystem>
#include <parser/parser.h>
#include <sstream>
#include <utils/symboltable.h>

#define MAX_MEM 16383
#define MAX_15BIT 32767

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        std::cout << "Usage: " << argv[0] << " filename.asm" << std::endl;
        return -1;
    }
    std::filesystem::path filePath(argv[1]);
    if (filePath.has_extension())
    {
        if (filePath.extension() == ".asm")
        {
            try
            {
                std::stringstream output;
                SymbolTable symTab;
                int instructionNumber = 0;
                int nextMemoryAddress = 16;
                Parser p(argv[1]);
                /** First pass. */
                while (p.hasMoreCommands())
                {
                    p.advance();
                    auto commType = p.commandType();
                    if (commType == Parser::A_COMMAND || commType == Parser::C_COMMAND)
                    {
                        instructionNumber++;
                    }
                    else if (p.commandType() == Parser::L_COMMAND)
                    {
                        std::string symbol = p.symbol();
                        if (!symTab.contains(symbol))
                        {
                            symTab.addEntry(symbol, instructionNumber);
                        }
                        else
                        {
                            throw DuplicateLabelException("In line " + std::to_string(p.currentLine()) + ": duplicate label found!");
                        }
                    }
                }
                /** Second pass. */
                p.rewind();
                while (p.hasMoreCommands())
                {
                    if (nextMemoryAddress > MAX_MEM)
                    {
                        throw OutOfMemoryException("Error: out of memory for symbols!");
                    }
                    p.advance();
                    auto commType = p.commandType();
                    if (commType == Parser::A_COMMAND)
                    {
                        std::string symbol = p.symbol();
                        if (!std::all_of(symbol.begin(), symbol.end(), ::isdigit))
                        {
                            int address;
                            if (symTab.contains(symbol))
                            {
                                address = symTab.getAddress(symbol);
                                output << "0" << std::bitset<15>(address).to_string() << std::endl;
                            }
                            else
                            {
                                address = nextMemoryAddress;
                                symTab.addEntry(symbol, address);
                                output << "0" << std::bitset<15>(address).to_string() << std::endl;
                                nextMemoryAddress++;
                            }
                        }
                        else
                        {
                            int constant = std::stoi(symbol);
                            if (constant > MAX_15BIT)
                            {
                                throw(std::runtime_error("Error: constant value bigger than maximum allowed of 32767!"));
                            }
                            output << "0" << std::bitset<15>(std::stoi(symbol)).to_string() << std::endl;
                        }
                    }
                    else if (commType == Parser::C_COMMAND)
                    {
                        output << "111" << Code::comp(p.comp()) << Code::dest(p.dest()) << Code::jump(p.jump()) << std::endl;
                    }
                }
                // write the output file
                std::ofstream outFile(filePath.replace_extension(".hack"));
                if (outFile.bad())
                {
                    throw std::runtime_error("Error while opening the output file!");
                }
                outFile << output.str();
            }
            catch (ParserException &ex)
            {
                std::cout << ex.what() << std::endl;
                return -1;
            }
            catch (DuplicateLabelException &ex)
            {
                std::cout << ex.what() << std::endl;
                return -1;
            }
            catch (OutOfMemoryException &ex)
            {
                std::cout << ex.what() << std::endl;
                return -1;
            }
            catch (std::runtime_error &ex)
            {
                std::cout << ex.what() << std::endl;
                return -1;
            }
        }
        else
        {
            std::cout << argv[1] << " is not a .asm file" << std::endl;
            return -1;
        }
    }
    else
    {
        std::cout << argv[1] << " is not a .asm file" << std::endl;
        return -1;
    }
    return 0;
}