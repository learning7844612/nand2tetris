@echo OFF

IF "%1" == "--setup" GOTO setup
IF "%1" == "--compile" GOTO compile
IF "%1" == "--clean-setup" GOTO clean-setup   
IF "%1" == "" GOTO error

:setup
    mkdir CMake
    mkdir bin
    cd CMake
    cmake ..
	GOTO finish

:compile
    cd CMake
    MSBuild.exe .\ALL_BUILD.vcxproj -p:Configuration=Release -t:Compile
	move .\Release\HackAssembler.exe ..\bin
	GOTO finish

:clean-setup
    rmdir /S /Q CMake
    rmdir /S /Q bin
	GOTO finish

:error
    echo "Usage: %0 --setup | --compile | --clean-setup"
	GOTO finish
	
:finish
	exit