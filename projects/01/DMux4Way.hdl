// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/01/DMux4Way.hdl
// This project has been done by Dimitri Schiavone<schiavone.dimitri.2@gmail.com>

/**
 * 4-way demultiplexor:
 * {a, b, c, d} = {in, 0, 0, 0} if sel == 00
 *                {0, in, 0, 0} if sel == 01
 *                {0, 0, in, 0} if sel == 10
 *                {0, 0, 0, in} if sel == 11
 */

CHIP DMux4Way {
    IN in, sel[2];
    OUT a, b, c, d;

    PARTS:
    // Put your code here:
    /* The logic we've used here is the same as for the Mux4WayX but in reverse. The least significant bit of sel
     * is used to choose between the 2 demultiplexers to use, wheras the most significant one is used to choose
     * between the possible outputs.
     */
    DMux(in=in, sel=sel[1], a=dmux1out, b=dmux2out);
    DMux(in=dmux1out, sel=sel[0], a=a, b=b);
    DMux(in=dmux2out, sel=sel[0], a=c, b=d);

}
