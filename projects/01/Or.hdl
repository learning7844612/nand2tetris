// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/01/Or.hdl
// This project has been done by Dimitri Schiavone<schiavone.dimitri.2@gmail.com>

 /**
 * Or gate:
 * out = 1 if (a == 1 or b == 1)
 *       0 otherwise
 */

CHIP Or {
    IN a, b;
    OUT out;

    PARTS:
    // Put your code here:
    /* To know how to do this, we need to refresh our boolean logic. Here it helps to rember the De Morgan's laws:
     * The first states that Not(And(A,B)) = Or(Not(A), Not(B)), the second states that Not(Or(A,B)) = And(Not(A),Not(B)).
     * What we need is Or(A,B), we can clearly see that starting from the first De Morgan's law we can negate both sides to get
     * the Or gate, which is what we do here. Since the book suggests to use previously built chips we use the already constructed Not gates.
     * Otherwise we could've used Nand gates with the same input instead of Not gates.
     */
    Not(in=a, out=nota);
    Not(in=b, out=notb);
    Nand(a=nota, b=notb, out=out);
}
