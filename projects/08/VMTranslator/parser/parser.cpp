#include <algorithm>
#include <exceptions/parserexception.h>
#include <fstream>
#include <iostream>
#include <lexer/lexer.h>
#include <lexer/token.h>
#include <parser/parser.h>
#include <string>

Parser::Parser(std::string inFileName) : inFile(std::ifstream(inFileName)), mCommandType(I_CMD), currentLineNumber(1)
{
    if (inFile.fail())
    {
        throw std::runtime_error("Error while opening file " + inFileName);
    }
}

Parser::~Parser()
{
    inFile.close();
}

bool Parser::hasMoreCommands()
{
    return inFile.good();
}

void Parser::removeSpaces()
{
    currentLine.erase(std::remove(currentLine.begin(), currentLine.end(), ' '), currentLine.end());
    currentLine.erase(std::remove(currentLine.begin(), currentLine.end(), '\t'), currentLine.end());
    currentLine.erase(std::remove(currentLine.begin(), currentLine.end(), '\r'), currentLine.end());
}

void Parser::removeComments()
{
    std::string::size_type index;
    index = currentLine.find("//");
    if (index != std::string::npos)
    {
        currentLine.erase(index);
    }
}

void Parser::checkIndex()
{
    int value = std::stoi(currentToken.mValue);
    if (mArg1 == "pointer")
    {
        if (value > 1)
        {
            throw ParserException("Line: " + std::to_string(currentLineNumber) + " invalid index value");
        }
    }
    else if (mArg1 == "temp")
    {
        if (value > 7)
        {
            throw ParserException("Line: " + std::to_string(currentLineNumber) + " invalid index value");
        }
    }
    else if (mArg1 == "constant")
    {
        if (value > 32767)
        {
            throw ParserException("Line: " + std::to_string(currentLineNumber) + " invalid index value");
        }
    }
}

bool Parser::parsePush(Lexer &lex)
{
    if (currentToken.mToken == Token::PUSH)
    {
        currentToken = lex.getToken();
        if (currentToken.mToken == Token::SEG)
        {
            mArg1 = currentToken.mValue;
            currentToken = lex.getToken();
            if (currentToken.mToken == Token::IDX)
            {
                checkIndex();
                mArg2 = std::stoi(currentToken.mValue);
                currentToken = lex.getToken();
                if (currentToken.mToken == Token::LEND)
                {
                    mCommandType = command::C_PUSH;
                    return true;
                }
                else
                {
                    throw ParserException("Line: " + std::to_string(currentLineNumber) + " unexpected input found at the end of the line");
                }
            }
            else
            {
                throw ParserException("Line: " + std::to_string(currentLineNumber) + " unexpected token found");
            }
        }
        else
        {
            throw ParserException("Line: " + std::to_string(currentLineNumber) + " unexpected token found");
        }
    }
    return false;
}

bool Parser::parsePop(Lexer &lex)
{
    if (currentToken.mToken == Token::POP)
    {
        currentToken = lex.getToken();
        if (currentToken.mToken == Token::SEG)
        {
            if (currentToken.mValue == "constant")
            {
                throw ParserException("Line: " + std::to_string(currentLineNumber) + " unexpected token found");
            }
            mArg1 = currentToken.mValue;
            currentToken = lex.getToken();
            if (currentToken.mToken == Token::IDX)
            {
                checkIndex();
                mArg2 = std::stoi(currentToken.mValue);
                currentToken = lex.getToken();
                if (currentToken.mToken == Token::LEND)
                {
                    mCommandType = command::C_POP;
                    return true;
                }
                else
                {
                    throw ParserException("Line: " + std::to_string(currentLineNumber) + " unexpected input found at the end of the line");
                }
            }
            else
            {
                throw ParserException("Line: " + std::to_string(currentLineNumber) + " unexpected token found");
            }
        }
        else
        {
            throw ParserException("Line: " + std::to_string(currentLineNumber) + " unexpected token found");
        }
    }
    return false;
}

bool Parser::parseArithmetic(Lexer &lex)
{
    if (currentToken.mToken == Token::ADD || currentToken.mToken == Token::SUB || currentToken.mToken == Token::NEG || currentToken.mToken == Token::EQ || currentToken.mToken == Token::GT || currentToken.mToken == Token::LT || currentToken.mToken == Token::AND || currentToken.mToken == Token::OR || currentToken.mToken == Token::NOT)
    {
        mArg1 = currentToken.mValue;
        currentToken = lex.getToken();
        if (currentToken.mToken == Token::LEND)
        {
            mCommandType = command::C_ARITHMETIC;
            return true;
        }
        else
        {
            throw ParserException("Line: " + std::to_string(currentLineNumber) + " unexpected input found at the end of the line");
        }
    }
    return false;
}

bool Parser::parseLabel(Lexer &lex)
{
    if (currentToken.mToken == Token::LBL)
    {
        currentToken = lex.getToken();
        if (currentToken.mToken == Token::SYM)
        {
            mArg1 = currentToken.mValue;
            currentToken = lex.getToken();
            if (currentToken.mToken == Token::LEND)
            {
                mCommandType = command::C_LABEL;
                return true;
            }
            else
            {
                throw ParserException("Line: " + std::to_string(currentLineNumber) + " unexpected input found at the end of the line");
            }
        }
        else
        {
            throw ParserException("Line: " + std::to_string(currentLineNumber) + " unexpected token found");
        }
    }
    return false;
}

bool Parser::parseGoto(Lexer &lex)
{
    if (currentToken.mToken == Token::GOTO)
    {
        currentToken = lex.getToken();
        if (currentToken.mToken == Token::SYM)
        {
            mArg1 = currentToken.mValue;
            currentToken = lex.getToken();
            if (currentToken.mToken == Token::LEND)
            {
                mCommandType = command::C_GOTO;
                return true;
            }
            else
            {
                throw ParserException("Line: " + std::to_string(currentLineNumber) + " unexpected input found at the end of the line");
            }
        }
        else
        {
            throw ParserException("Line: " + std::to_string(currentLineNumber) + " unexpected token found");
        }
    }
    return false;
}

bool Parser::parseIfGoto(Lexer &lex)
{
    if (currentToken.mToken == Token::IF)
    {
        currentToken = lex.getToken();
        if (currentToken.mToken == Token::MINUS)
        {
            currentToken = lex.getToken();
            if (currentToken.mToken == Token::GOTO)
            {
                currentToken = lex.getToken();
                if (currentToken.mToken == Token::SYM)
                {
                    mArg1 = currentToken.mValue;
                    currentToken = lex.getToken();
                    if (currentToken.mToken == Token::LEND)
                    {
                        mCommandType = command::C_IF;
                        return true;
                    }
                    else
                    {
                        throw ParserException("Line: " + std::to_string(currentLineNumber) + " unexpected input found at the end of the line");
                    }
                }
                else
                {
                    throw ParserException("Line: " + std::to_string(currentLineNumber) + " unexpected token found");
                }
            }
            else
            {
                throw ParserException("Line: " + std::to_string(currentLineNumber) + " unexpected token found");
            }
        }
        else
        {
            throw ParserException("Line: " + std::to_string(currentLineNumber) + " unexpected token found");
        }
    }
    return false;
}

bool Parser::parseFunction(Lexer &lex)
{
    if (currentToken.mToken == Token::FUNC)
    {
        currentToken = lex.getToken();
        if (currentToken.mToken == Token::SYM)
        {
            mArg1 = currentToken.mValue;
            currentToken = lex.getToken();
            if (currentToken.mToken == Token::IDX)
            {
                mArg2 = std::stoi(currentToken.mValue);
                currentToken = lex.getToken();
                if (currentToken.mToken == Token::LEND)
                {
                    mCommandType = command::C_FUNCTION;
                    return true;
                }
                else
                {
                    throw ParserException("Line: " + std::to_string(currentLineNumber) + " unexpected input found at the end of the line");
                }
            }
            else
            {
                throw ParserException("Line: " + std::to_string(currentLineNumber) + " unexpected token found");
            }
        }
        else
        {
            throw ParserException("Line: " + std::to_string(currentLineNumber) + " unexpected token found");
        }
    }
    return false;
}

bool Parser::parseCall(Lexer &lex)
{
    if (currentToken.mToken == Token::CALL)
    {
        currentToken = lex.getToken();
        if (currentToken.mToken == Token::SYM)
        {
            mArg1 = currentToken.mValue;
            currentToken = lex.getToken();
            if (currentToken.mToken == Token::IDX)
            {
                mArg2 = std::stoi(currentToken.mValue);
                currentToken = lex.getToken();
                if (currentToken.mToken == Token::LEND)
                {
                    mCommandType = command::C_CALL;
                    return true;
                }
                else
                {
                    throw ParserException("Line: " + std::to_string(currentLineNumber) + " unexpected input found at the end of the line");
                }
            }
            else
            {
                throw ParserException("Line: " + std::to_string(currentLineNumber) + " unexpected token found");
            }
        }
        else
        {
            throw ParserException("Line: " + std::to_string(currentLineNumber) + " unexpected token found " + std::to_string(currentToken.mToken) + currentToken.mValue);
        }
    }
    return false;
}

bool Parser::parseReturn(Lexer &lex)
{
    if (currentToken.mToken == Token::RET)
    {
        currentToken = lex.getToken();
        if (currentToken.mToken == Token::LEND)
        {
            mCommandType = command::C_RETURN;
            return true;
        }
        else
        {
            throw ParserException("Line: " + std::to_string(currentLineNumber) + " unexpected input found at the end of the line");
        }
    }
    return false;
}

void Parser::parse(Lexer &lex)
{
    if (parsePush(lex))
    {
        return;
    }
    else if (parsePop(lex))
    {
        return;
    }
    else if (parseArithmetic(lex))
    {
        return;
    }
    else if (parseLabel(lex))
    {
        return;
    }
    else if (parseGoto(lex))
    {
        return;
    }
    else if (parseIfGoto(lex))
    {
        return;
    }
    else if (parseFunction(lex))
    {
        return;
    }
    else if (parseCall(lex))
    {
        return;
    }
    else if (parseReturn(lex))
    {
        return;
    }
    else
    {
        throw ParserException("Line: " + std::to_string(currentLineNumber) + " invalid command found");
    }
}

void Parser::advance()
{
    mCommandType = command::I_CMD;
    mArg1.clear();
    mArg2 = 0;
    std::getline(inFile, currentLine);
    while (currentLine.empty())
    {
        std::getline(inFile, currentLine);
        currentLineNumber++;
        if (inFile.eof())
        {
            return;
        }
    }
    removeSpaces();
    removeComments();
    if (!currentLine.empty())
    {
        Lexer lex = Lexer(currentLine);
        currentToken = lex.getToken();
        parse(lex);
    }
    currentLineNumber++;
}

Parser::command Parser::commandType() const
{
    return mCommandType;
}

const std::string &Parser::arg1() const
{
    return mArg1;
}

int Parser::arg2() const
{
    return mArg2;
}