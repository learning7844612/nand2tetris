#include <lexer/lexer.h>
#include <lexer/token.h>
#include <string>

Lexer::Lexer() {}
Lexer::Lexer(std::string &line) : mLine(line), mLineIterator(mLine.begin()), prevToken(Token::INV) {}

Token Lexer::getToken()
{
    std::string lineBuffer;
    if (mLineIterator == mLine.end())
    {
        return Token(Token::LEND, lineBuffer);
    }
    while (std::isalpha(*mLineIterator))
    {
        lineBuffer += *mLineIterator++;
        if (lineBuffer == "push")
        {
            return Token(Token::PUSH, lineBuffer);
        }
        else if (lineBuffer == "pop")
        {
            return Token(Token::POP, lineBuffer);
        }
        else if (lineBuffer == "argument")
        {
            return Token(Token::SEG, lineBuffer);
        }
        else if (lineBuffer == "local")
        {
            return Token(Token::SEG, lineBuffer);
        }
        else if (lineBuffer == "static")
        {
            return Token(Token::SEG, lineBuffer);
        }
        else if (lineBuffer == "constant")
        {
            return Token(Token::SEG, lineBuffer);
        }
        else if (lineBuffer == "this")
        {
            return Token(Token::SEG, lineBuffer);
        }
        else if (lineBuffer == "that")
        {
            return Token(Token::SEG, lineBuffer);
        }
        else if (lineBuffer == "pointer")
        {
            return Token(Token::SEG, lineBuffer);
        }
        else if (lineBuffer == "temp")
        {
            return Token(Token::SEG, lineBuffer);
        }
        else if (lineBuffer == "add")
        {
            return Token(Token::ADD, lineBuffer);
        }
        else if (lineBuffer == "sub")
        {
            return Token(Token::SUB, lineBuffer);
        }
        else if (lineBuffer == "neg")
        {
            return Token(Token::NEG, lineBuffer);
        }
        else if (lineBuffer == "eq")
        {
            return Token(Token::EQ, lineBuffer);
        }
        else if (lineBuffer == "gt")
        {
            prevToken = Token::GT;
            return Token(Token::GT, lineBuffer);
        }
        else if (lineBuffer == "lt")
        {
            return Token(Token::LT, lineBuffer);
        }
        else if (lineBuffer == "and")
        {
            return Token(Token::AND, lineBuffer);
        }
        else if (lineBuffer == "or")
        {
            prevToken = Token::OR;
            return Token(Token::OR, lineBuffer);
        }
        else if (lineBuffer == "not")
        {
            return Token(Token::NOT, lineBuffer);
        }
        else if (lineBuffer == "label")
        {
            return Token(Token::LBL, lineBuffer);
        }
        else if (lineBuffer == "goto")
        {
            return Token(Token::GOTO, lineBuffer);
        }
        else if (lineBuffer == "function")
        {
            prevToken = Token::FUNC;
            return Token(Token::FUNC, lineBuffer);
        }
        else if (lineBuffer == "call")
        {
            prevToken = Token::CALL;
            return Token(Token::CALL, lineBuffer);
        }
        else if (lineBuffer == "return")
        {
            return Token(Token::RET, lineBuffer);
        }
        else if (lineBuffer == "if")
        {
            return Token(Token::IF, lineBuffer);
        }
    }
    if (*mLineIterator == '_' || *mLineIterator == '.' || *mLineIterator == ':')
    {
        lineBuffer += *mLineIterator++;
        while (std::isalnum(*mLineIterator) || *mLineIterator == '.' || *mLineIterator == ':' || *mLineIterator == '_')
        {
            if ((prevToken == Token::FUNC || prevToken == Token::CALL) && ((mLineIterator + 1) == mLine.end()))
            {
                return Token(Token::SYM, lineBuffer);
            }
            else
            {
                lineBuffer += *mLineIterator++;
            }
        }
        return Token(Token::SYM, lineBuffer);
    }
    else if (*mLineIterator == '-')
    {
        lineBuffer = *mLineIterator++;
        return Token(Token::MINUS, lineBuffer);
    }
    else if (std::isdigit(*mLineIterator))
    {
        lineBuffer += *mLineIterator++;
        while (std::isdigit(*mLineIterator))
        {
            lineBuffer += *mLineIterator++;
        }
        if (mLineIterator == mLine.end())
        {
            return Token(Token::IDX, lineBuffer);
        }
        else
        {
            while (std::isalpha(*mLineIterator) || *mLineIterator == '.' || *mLineIterator == ':' || *mLineIterator == '_')
            {
                lineBuffer += *mLineIterator++;
            }
            return Token(Token::SYM, lineBuffer);
        }
    }
    else
    {
        return Token(Token::SYM, lineBuffer);
    }
}