#include <algorithm>
#include <code/codewriter.h>
#include <exceptions/outofmemoryexception.h>
#include <fstream>
#include <iostream>
#include <list>
#include <parser/parser.h>
#include <sstream>
#include <string>

#define STATIC_SEGMENT_MAX 239

CodeWriter::CodeWriter(std::string outFileName) : mOutFileName(outFileName)
{
    staticVarList = nullptr;
    eqLabelCounter = 0;
    gtLabelCounter = 0;
    ltLabelCounter = 0;
    retLabelCounter = 0;
    writeInit();
}

CodeWriter::~CodeWriter()
{
    outFile.close();
    delete staticVarList;
}

void CodeWriter::setFileName(std::string fileName)
{
    if (staticVarList)
    {
        delete staticVarList;
    }
    staticVarList = new std::list<std::string>;
    currentFileName = fileName;
    currentFunctionName = currentFileName + "$NO_FUNC";
    eqLabelCounter = 0;
    gtLabelCounter = 0;
    ltLabelCounter = 0;
    retLabelCounter = 0;
    output << "// Begin file " << fileName << ".vm" << std::endl;
}

void CodeWriter::writeArithmetic(std::string command)
{
    if (command == "add")
    {
        output << "// add" << std::endl
               << "@SP" << std::endl
               << "AM=M-1" << std::endl
               << "D=M" << std::endl
               << "A=A-1" << std::endl
               << "D=D+M" << std::endl
               << "M=D" << std::endl;
    }
    else if (command == "sub")
    {
        output << "// sub" << std::endl
               << "@SP" << std::endl
               << "AM=M-1" << std::endl
               << "D=M" << std::endl
               << "A=A-1" << std::endl
               << "D=M-D" << std::endl
               << "M=D" << std::endl;
    }
    else if (command == "neg")
    {
        output << "// neg" << std::endl
               << "@SP" << std::endl
               << "A=M-1" << std::endl
               << "D=M" << std::endl
               << "D=!D" << std::endl
               << "D=D+1" << std::endl
               << "M=D" << std::endl;
    }
    else if (command == "eq")
    {
        output << "// eq" << std::endl
               << "@SP" << std::endl
               << "AM=M-1" << std::endl
               << "D=M" << std::endl
               << "A=A-1" << std::endl
               << "D=M-D" << std::endl
               << "@" << currentFunctionName << "$NEQ." << std::to_string(eqLabelCounter) << std::endl
               << "D;JNE" << std::endl
               << "@SP" << std::endl
               << "A=M-1" << std::endl
               << "M=-1" << std::endl
               << "@" << currentFunctionName << "$EQ." << std::to_string(eqLabelCounter) << std::endl
               << "0;JMP" << std::endl
               << "(" << currentFunctionName << "$NEQ." << std::to_string(eqLabelCounter) << ")" << std::endl
               << "@SP" << std::endl
               << "A=M-1" << std::endl
               << "M=0" << std::endl
               << "(" << currentFunctionName << "$EQ." << std::to_string(eqLabelCounter) << ")" << std::endl;
        eqLabelCounter++;
    }
    else if (command == "gt")
    {
        output << "// gt" << std::endl
               << "@SP" << std::endl
               << "AM=M-1" << std::endl
               << "D=M" << std::endl
               << "A=A-1" << std::endl
               << "D=M-D" << std::endl
               << "@" << currentFunctionName << "$NGT." << std::to_string(gtLabelCounter) << std::endl
               << "D;JLE" << std::endl
               << "@SP" << std::endl
               << "A=M-1" << std::endl
               << "M=-1" << std::endl
               << "@" << currentFunctionName << "$GT." << std::to_string(gtLabelCounter) << std::endl
               << "0;JMP" << std::endl
               << "(" << currentFunctionName << "$NGT." << std::to_string(gtLabelCounter) << ")" << std::endl
               << "@SP" << std::endl
               << "A=M-1" << std::endl
               << "M=0" << std::endl
               << "(" << currentFunctionName << "$GT." << std::to_string(gtLabelCounter) << ")" << std::endl;
        gtLabelCounter++;
    }
    else if (command == "lt")
    {
        output << "// lt" << std::endl
               << "@SP" << std::endl
               << "AM=M-1" << std::endl
               << "D=M" << std::endl
               << "A=A-1" << std::endl
               << "D=M-D" << std::endl
               << "@" << currentFunctionName << "$NLT." << std::to_string(ltLabelCounter) << std::endl
               << "D;JGE" << std::endl
               << "@SP" << std::endl
               << "A=M-1" << std::endl
               << "M=-1" << std::endl
               << "@" << currentFunctionName << "$LT." << std::to_string(ltLabelCounter) << std::endl
               << "0;JMP" << std::endl
               << "(" << currentFunctionName << "$NLT." << std::to_string(ltLabelCounter) << ")" << std::endl
               << "@SP" << std::endl
               << "A=M-1" << std::endl
               << "M=0" << std::endl
               << "(" << currentFunctionName << "$LT." << std::to_string(ltLabelCounter) << ")" << std::endl;
        ltLabelCounter++;
    }
    else if (command == "and")
    {
        output << "// and" << std::endl
               << "@SP" << std::endl
               << "AM=M-1" << std::endl
               << "D=M" << std::endl
               << "@SP" << std::endl
               << "A=M-1" << std::endl
               << "D=D&M" << std::endl
               << "M=D" << std::endl;
    }
    else if (command == "or")
    {
        output << "// or" << std::endl
               << "@SP" << std::endl
               << "AM=M-1" << std::endl
               << "D=M" << std::endl
               << "@SP" << std::endl
               << "A=M-1" << std::endl
               << "D=D|M" << std::endl
               << "M=D" << std::endl;
    }
    else if (command == "not")
    {
        output << "// not" << std::endl
               << "@SP" << std::endl
               << "A=M-1" << std::endl
               << "D=M" << std::endl
               << "D=!D" << std::endl
               << "M=D" << std::endl;
    }
}

void CodeWriter::writePushPop(Parser::command commandType, std::string segment, int index)
{
    if (commandType == Parser::C_PUSH)
    {
        if (segment == "constant")
        {
            output << "// push constant " << std::to_string(index) << std::endl
                   << "@" << std::to_string(index) << std::endl
                   << "D=A" << std::endl
                   << "@SP" << std::endl
                   << "A=M" << std::endl
                   << "M=D" << std::endl
                   << "@SP" << std::endl
                   << "M=M+1" << std::endl;
        }
        else if (segment == "local")
        {
            output << "// push local " << std::to_string(index) << std::endl
                   << "@" << std::to_string(index) << std::endl
                   << "D=A" << std::endl
                   << "@LCL" << std::endl
                   << "A=M" << std::endl
                   << "A=D+A" << std::endl
                   << "D=M" << std::endl
                   << "@SP" << std::endl
                   << "A=M" << std::endl
                   << "M=D" << std::endl
                   << "@SP" << std::endl
                   << "M=M+1" << std::endl;
        }
        else if (segment == "argument")
        {
            output << "// push argument " << std::to_string(index) << std::endl
                   << "@" << std::to_string(index) << std::endl
                   << "D=A" << std::endl
                   << "@ARG" << std::endl
                   << "A=M" << std::endl
                   << "A=D+A" << std::endl
                   << "D=M" << std::endl
                   << "@SP" << std::endl
                   << "A=M" << std::endl
                   << "M=D" << std::endl
                   << "@SP" << std::endl
                   << "M=M+1" << std::endl;
        }
        else if (segment == "this")
        {
            output << "// push this " << std::to_string(index) << std::endl
                   << "@" << std::to_string(index) << std::endl
                   << "D=A" << std::endl
                   << "@THIS" << std::endl
                   << "A=M" << std::endl
                   << "A=D+A" << std::endl
                   << "D=M" << std::endl
                   << "@SP" << std::endl
                   << "A=M" << std::endl
                   << "M=D" << std::endl
                   << "@SP" << std::endl
                   << "M=M+1" << std::endl;
        }
        else if (segment == "that")
        {
            output << "// push that " << std::to_string(index) << std::endl
                   << "@" << std::to_string(index) << std::endl
                   << "D=A" << std::endl
                   << "@THAT" << std::endl
                   << "A=M" << std::endl
                   << "A=D+A" << std::endl
                   << "D=M" << std::endl
                   << "@SP" << std::endl
                   << "A=M" << std::endl
                   << "M=D" << std::endl
                   << "@SP" << std::endl
                   << "M=M+1" << std::endl;
        }
        else if (segment == "pointer")
        {
            output << "// push pointer " << std::to_string(index) << std::endl
                   << "@" << std::to_string(index) << std::endl
                   << "D=A" << std::endl
                   << "@3" << std::endl
                   << "A=D+A" << std::endl
                   << "D=M" << std::endl
                   << "@SP" << std::endl
                   << "A=M" << std::endl
                   << "M=D" << std::endl
                   << "@SP" << std::endl
                   << "M=M+1" << std::endl;
        }
        else if (segment == "temp")
        {
            output << "// push temp " << std::to_string(index) << std::endl
                   << "@" << std::to_string(index) << std::endl
                   << "D=A" << std::endl
                   << "@5" << std::endl
                   << "A=D+A" << std::endl
                   << "D=M" << std::endl
                   << "@SP" << std::endl
                   << "A=M" << std::endl
                   << "M=D" << std::endl
                   << "@SP" << std::endl
                   << "M=M+1" << std::endl;
        }
        else if (segment == "static")
        {
            if (staticVarCounter > STATIC_SEGMENT_MAX)
            {
                throw OutOfMemoryException("out of memory for static storage");
            }
            output << "// push static " << std::to_string(index) << std::endl
                   << "@" << currentFileName << "." << index << std::endl
                   << "D=M" << std::endl
                   << "@SP" << std::endl
                   << "A=M" << std::endl
                   << "M=D" << std::endl
                   << "@SP" << std::endl
                   << "M=M+1" << std::endl;
            std::string staticVar = currentFileName + "." + std::to_string(index);
            if (!staticVarExists(staticVar))
            {
                staticVarList->push_back(staticVar);
                staticVarCounter++;
            }
        }
    }
    else if (commandType == Parser::C_POP)
    {
        if (segment == "local")
        {
            output << "// pop local " << std::to_string(index) << std::endl
                   << "@" << std::to_string(index) << std::endl
                   << "D=A" << std::endl
                   << "@LCL" << std::endl
                   << "A=M" << std::endl
                   << "D=D+A" << std::endl
                   << "@R13" << std::endl
                   << "M=D" << std::endl
                   << "@SP" << std::endl
                   << "AM=M-1" << std::endl
                   << "D=M" << std::endl
                   << "@R13" << std::endl
                   << "A=M" << std::endl
                   << "M=D" << std::endl;
        }
        else if (segment == "argument")
        {
            output << "// pop argument " << std::to_string(index) << std::endl
                   << "@" << std::to_string(index) << std::endl
                   << "D=A" << std::endl
                   << "@ARG" << std::endl
                   << "A=M" << std::endl
                   << "D=D+A" << std::endl
                   << "@R13" << std::endl
                   << "M=D" << std::endl
                   << "@SP" << std::endl
                   << "AM=M-1" << std::endl
                   << "D=M" << std::endl
                   << "@R13" << std::endl
                   << "A=M" << std::endl
                   << "M=D" << std::endl;
        }
        else if (segment == "this")
        {
            output << "// pop this " << std::to_string(index) << std::endl
                   << "@" << std::to_string(index) << std::endl
                   << "D=A" << std::endl
                   << "@THIS" << std::endl
                   << "A=M" << std::endl
                   << "D=D+A" << std::endl
                   << "@R13" << std::endl
                   << "M=D" << std::endl
                   << "@SP" << std::endl
                   << "AM=M-1" << std::endl
                   << "D=M" << std::endl
                   << "@R13" << std::endl
                   << "A=M" << std::endl
                   << "M=D" << std::endl;
        }
        else if (segment == "that")
        {
            output << "// pop that " << std::to_string(index) << std::endl
                   << "@" << std::to_string(index) << std::endl
                   << "D=A" << std::endl
                   << "@THAT" << std::endl
                   << "A=M" << std::endl
                   << "D=D+A" << std::endl
                   << "@R13" << std::endl
                   << "M=D" << std::endl
                   << "@SP" << std::endl
                   << "AM=M-1" << std::endl
                   << "D=M" << std::endl
                   << "@R13" << std::endl
                   << "A=M" << std::endl
                   << "M=D" << std::endl;
        }
        else if (segment == "pointer")
        {
            output << "// pop pointer " << std::to_string(index) << std::endl
                   << "@" << std::to_string(index) << std::endl
                   << "D=A" << std::endl
                   << "@3" << std::endl
                   << "D=D+A" << std::endl
                   << "@R13" << std::endl
                   << "M=D" << std::endl
                   << "@SP" << std::endl
                   << "AM=M-1" << std::endl
                   << "D=M" << std::endl
                   << "@R13" << std::endl
                   << "A=M" << std::endl
                   << "M=D" << std::endl;
        }
        else if (segment == "temp")
        {
            output << "// pop temp " << std::to_string(index) << std::endl
                   << "@" << std::to_string(index) << std::endl
                   << "D=A" << std::endl
                   << "@5" << std::endl
                   << "D=D+A" << std::endl
                   << "@R13" << std::endl
                   << "M=D" << std::endl
                   << "@SP" << std::endl
                   << "AM=M-1" << std::endl
                   << "D=M" << std::endl
                   << "@R13" << std::endl
                   << "A=M" << std::endl
                   << "M=D" << std::endl;
        }
        else if (segment == "static")
        {
            if (staticVarCounter > STATIC_SEGMENT_MAX)
            {
                throw OutOfMemoryException("out of memory for static storage");
            }
            output << "// pop static " << std::to_string(index) << std::endl
                   << "@SP" << std::endl
                   << "AM=M-1" << std::endl
                   << "D=M" << std::endl
                   << "@" << currentFileName << "." << index << std::endl
                   << "M=D" << std::endl;
            std::string staticVar = currentFileName + "." + std::to_string(index);
            if (!staticVarExists(staticVar))
            {
                staticVarList->push_back(staticVar);
                staticVarCounter++;
            }
        }
    }
}

void CodeWriter::writeInit()
{
    output << "// bootstrap code " << std::endl
           << "@256" << std::endl
           << "D=A" << std::endl
           << "@SP" << std::endl
           << "M=D" << std::endl;
    writeCall("Sys.init", 0);
}

void CodeWriter::writeLabel(std::string label)
{
    output << "// label " << label << std::endl
           << "(" << currentFunctionName << "$" << label << ")" << std::endl;
}

void CodeWriter::writeGoto(std::string label)
{
    output << "// goto " << label << std::endl
           << "@" << currentFunctionName << "$" << label << std::endl
           << "0;JMP" << std::endl;
}

void CodeWriter::writeIf(std::string label)
{
    output << "// if-goto " << label << std::endl
           << "@SP" << std::endl
           << "AM=M-1" << std::endl
           << "D=M" << std::endl
           << "@" << currentFunctionName << "$" << label << std::endl
           << "D;JNE" << std::endl;
}

void CodeWriter::writeFunction(std::string functionName, int nArgs)
{
    currentFunctionName = functionName;
    eqLabelCounter = 0;
    gtLabelCounter = 0;
    ltLabelCounter = 0;
    retLabelCounter = 0;
    output << "// function " << functionName << " " << std::to_string(nArgs) << std::endl
           << "(" << functionName << ")" << std::endl;
    for (int i = 0; i < nArgs; i++)
    {
        output << "@0" << std::endl
               << "D=A" << std::endl
               << "@SP" << std::endl
               << "A=M" << std::endl
               << "M=D" << std::endl
               << "@SP" << std::endl
               << "M=M+1" << std::endl;
    }
}

void CodeWriter::writeCall(std::string functionName, int nArgs)
{
    output << "// call " << functionName << " " << std::to_string(nArgs) << std::endl
           << "@" << currentFunctionName << "$RET." << retLabelCounter << std::endl
           << "D=A" << std::endl
           << "@SP" << std::endl
           << "A=M" << std::endl
           << "M=D" << std::endl
           << "@SP" << std::endl
           << "M=M+1" << std::endl
           << "@LCL" << std::endl
           << "D=M" << std::endl
           << "@SP" << std::endl
           << "A=M" << std::endl
           << "M=D" << std::endl
           << "@SP" << std::endl
           << "M=M+1" << std::endl
           << "@ARG" << std::endl
           << "D=M" << std::endl
           << "@SP" << std::endl
           << "A=M" << std::endl
           << "M=D" << std::endl
           << "@SP" << std::endl
           << "M=M+1" << std::endl
           << "@THIS" << std::endl
           << "D=M" << std::endl
           << "@SP" << std::endl
           << "A=M" << std::endl
           << "M=D" << std::endl
           << "@SP" << std::endl
           << "M=M+1" << std::endl
           << "@THAT" << std::endl
           << "D=M" << std::endl
           << "@SP" << std::endl
           << "A=M" << std::endl
           << "M=D" << std::endl
           << "@SP" << std::endl
           << "M=M+1" << std::endl
           << "@" << std::to_string(nArgs) << std::endl
           << "D=A" << std::endl
           << "@SP" << std::endl
           << "A=M" << std::endl
           << "D=A-D" << std::endl
           << "@5" << std::endl
           << "D=D-A" << std::endl
           << "@ARG" << std::endl
           << "M=D" << std::endl
           << "@SP" << std::endl
           << "D=M" << std::endl
           << "@LCL" << std::endl
           << "M=D" << std::endl
           << "@" << functionName << std::endl
           << "0;JMP" << std::endl
           << "(" << currentFunctionName << "$RET." << retLabelCounter << ")" << std::endl;
    retLabelCounter++;
}

void CodeWriter::writeReturn()
{
    output << "// return" << std::endl
           << "@LCL" << std::endl
           << "D=M" << std::endl
           << "@R13" << std::endl
           << "M=D" << std::endl
           << "@5" << std::endl
           << "A=D-A" << std::endl
           << "D=M" << std::endl
           << "@R14" << std::endl
           << "M=D" << std::endl
           << "@SP" << std::endl
           << "AM=M-1" << std::endl
           << "D=M" << std::endl
           << "@ARG" << std::endl
           << "A=M" << std::endl
           << "M=D" << std::endl
           << "@ARG" << std::endl
           << "A=M+1" << std::endl
           << "D=A" << std::endl
           << "@SP" << std::endl
           << "M=D" << std::endl
           << "@R13" << std::endl
           << "D=M" << std::endl
           << "A=D-1" << std::endl
           << "D=M" << std::endl
           << "@THAT" << std::endl
           << "M=D" << std::endl
           << "@R13" << std::endl
           << "D=M" << std::endl
           << "@2" << std::endl
           << "A=D-A" << std::endl
           << "D=M" << std::endl
           << "@THIS" << std::endl
           << "M=D" << std::endl
           << "@R13" << std::endl
           << "D=M" << std::endl
           << "@3" << std::endl
           << "A=D-A" << std::endl
           << "D=M" << std::endl
           << "@ARG" << std::endl
           << "M=D" << std::endl
           << "@R13" << std::endl
           << "D=M" << std::endl
           << "@4" << std::endl
           << "A=D-A" << std::endl
           << "D=M" << std::endl
           << "@LCL" << std::endl
           << "M=D" << std::endl
           << "@R14" << std::endl
           << "A=M" << std::endl
           << "0;JMP" << std::endl;
}

bool CodeWriter::staticVarExists(std::string var)
{
    auto listIterator = std::find(staticVarList->begin(), staticVarList->end(), var);
    if (listIterator != staticVarList->end())
    {
        return true;
    }
    return false;
}

void CodeWriter::writeOut()
{
    outFile = std::ofstream(mOutFileName);
    if (outFile.fail())
    {
        throw std::runtime_error("Error while opening file " + mOutFileName);
    }
    outFile << output.str();
}