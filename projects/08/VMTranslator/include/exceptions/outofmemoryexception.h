#ifndef OUTOFMEMORYEXCEPTION_H
#define OUTOFMEMORYEXCEPTION_H

#include <exception>
#include <iostream>
#include <string>

class OutOfMemoryException : public std::exception
{
    std::string _msg;

public:
    OutOfMemoryException(const std::string &msg) : _msg(msg) {}
    virtual const char *what() const noexcept override
    {
        return _msg.c_str();
    }
};

#endif /** OUTOFMEMORYEXCEPTION_H */