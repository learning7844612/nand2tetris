#ifndef TOKEN_H
#define TOKEN_H

#include <string>

class Token
{
public:
    enum token
    {
        PUSH,
        POP,
        ADD,
        SUB,
        NEG,
        EQ,
        GT,
        LT,
        AND,
        OR,
        NOT,
        SEG,
        IDX,
        SPACE,
        LBL,
        GOTO,
        FUNC,
        CALL,
        RET,
        IF,
        MINUS,
        SYM,
        LEND,
        INV
    };
    Token(void);
    Token(enum token, std::string);
    token mToken;
    std::string mValue;
};

#endif /** TOKEN_H */