#ifndef LEXER_H
#define LEXER_H

#include <fstream>
#include <string>
#include <lexer/token.h>

class Lexer
{
public:
    Lexer(void);
    Lexer(std::string &);
    Token getToken(void);

private:
    std::string mLine;
    std::string::iterator mLineIterator;
    int mColumn;
    Token::token prevToken;
};

#endif /** LEXER_H */