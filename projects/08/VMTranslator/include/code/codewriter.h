#ifndef CODEWRITER_H
#define CODEWRITER_H

#include <fstream>
#include <list>
#include <parser/parser.h>
#include <sstream>
#include <string>

class CodeWriter
{
public:
    CodeWriter(std::string);
    ~CodeWriter(void);
    void setFileName(std::string);
    void writeArithmetic(std::string);
    void writePushPop(Parser::command, std::string, int);
    void writeInit();
    void writeLabel(std::string);
    void writeGoto(std::string);
    void writeIf(std::string);
    void writeCall(std::string, int);
    void writeReturn(void);
    void writeFunction(std::string, int);
    void writeOut(void);

private:
    bool staticVarExists(std::string);
    std::ofstream outFile;
    std::string mOutFileName;
    std::string currentFileName;
    std::string currentFunctionName;
    int eqLabelCounter;
    int gtLabelCounter;
    int ltLabelCounter;
    int retLabelCounter;
    int staticVarCounter;
    std::list<std::string> *staticVarList;
    std::stringstream output;
};

#endif /** CODEWRITER_H */