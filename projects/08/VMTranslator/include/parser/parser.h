#ifndef PARSER_H
#define PARSER_H

#include <lexer/lexer.h>
#include <lexer/token.h>
#include <fstream>
#include <string>

class Parser
{
public:
    Parser(std::string);
    ~Parser(void);
    bool hasMoreCommands(void);
    void advance(void);
    enum command
    {
        C_ARITHMETIC,
        C_PUSH,
        C_POP,
        C_LABEL,
        C_GOTO,
        C_IF,
        C_FUNCTION,
        C_RETURN,
        C_CALL,
        I_CMD
    };
    command commandType(void) const;
    const std::string& arg1(void) const;
    int arg2(void) const;

private:
    void removeSpaces(void);
    void removeComments(void);
    bool parsePush(Lexer &);
    bool parsePop(Lexer &);
    bool parseArithmetic(Lexer &);
    bool parseLabel(Lexer &);
    bool parseGoto(Lexer &);
    bool parseIfGoto(Lexer &);
    bool parseFunction(Lexer &);
    bool parseCall(Lexer &);
    bool parseReturn(Lexer &);
    void parse(Lexer &);
    void checkIndex(void);
    std::ifstream inFile;
    std::string mArg1;
    int mArg2;
    command mCommandType;
    std::string currentLine;
    int currentLineNumber;
    int currentColumnNumber;
    Token currentToken;
};

#endif /** PARSER_H */