// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm
// This project has been done by Dimitri Schiavone<schiavone.dimitri.2@gmail.com>

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed.
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

// Put your code here.
// The logic of this program corresponds to the following snippet of c code:
//int i = 0;
//int *screenStart;
//int *keyboard;
//screenstart = 16384;
//keyboard = 24576;
//while(1){
//    while(*keyboard){
//        for(i = screenstart; i < keyboard; i++){
//            *screen = -1;
//            if(!(*keyboard)){
//                break;
//            }
//        }
//    }
//    while(!(*keyboard)){
//        for(i = screenstart; i < keyboard; i++){
//            *screen = 0;
//            if(*keyboard){
//                break;
//            }
//       }
//    }
//}
@i
M=0
(while0begin)
    (while1begin)
        @KBD
        D=M
        @while1end
        D;JEQ
        @16384
        D=A
        @i
        M=D
        (for0begin)
            @24576
            D=D-A
            @for0end
            D;JGE
            @i
            A=M
            M=-1
            @KBD
            D=M
            @while2begin
            D;JEQ
            @i
            D=M
            D=D+1
            @i
            M=D
            @for0begin
            0;JMP
        (for0end)
        @while1begin
        0;JMP
    (while1end)
    (while2begin)
    @KBD
    D=M
    @while2end
    D;JGT
    @16384
    D=A
    @i
    M=D
    (for1begin)
        @24576
        D=D-A
        @for1end
        D;JGE
        @i
        A=M
        M=0
        @KBD
        D=M
        @while0begin
        D;JGT
        @i
        D=M
        D=D+1
        @i
        M=D
        @for1begin
        0;JMP
    (for1end)
    @while1begin
    0;JMP
    (while2end)
@while0begin
0;JMP
