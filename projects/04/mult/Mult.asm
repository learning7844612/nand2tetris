// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Mult.asm
// This project has been done by Dimitri Schiavone<schiavone.dimitri.2@gmail.com>

// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)

// Put your code here.
// The following code is equivalent to this high level code in c syntax:
// int res, R2 = 0;
// int i;
// for(i=0; i<R1; i++){
//     res += R0;
// }
// R2 = res;
@res
M=0                                     //int res = 0;
@i
M=0                                     //int i = 0;
@R2
M=0
(beginfor)
@i
D=M
@R1
D=D-M                                   //i - R1;
@endfor
D;JGE                                   //i - R1 >= 0; ?
@res
D=M
@R0
D=D+M                                   //res + R0;
@res
M=D                                     //res = res + R0;
@i
D=M+1                                   //i + 1;
M=D                                     //i = i + 1;
@beginfor
0;JMP

(endfor)
@res
D=M
@R2
M=D                                     //R2 = res;
(end)
@end
0;JMP
