#include <lexer/lexer.h>
#include <lexer/token.h>
#include <string>

Lexer::Lexer() {}
Lexer::Lexer(std::string &line) : mLine(line), mLineIterator(mLine.begin()) {}

Token Lexer::getToken()
{
    std::string lineBuffer;
    while (std::isalpha(*mLineIterator))
    {
        lineBuffer += *mLineIterator++;
        if (lineBuffer == "push")
        {
            return Token(Token::PUSH, lineBuffer);
        }
        else if (lineBuffer == "pop")
        {
            return Token(Token::POP, lineBuffer);
        }
        else if (lineBuffer == "argument")
        {
            return Token(Token::SEG, lineBuffer);
        }
        else if (lineBuffer == "local")
        {
            return Token(Token::SEG, lineBuffer);
        }
        else if (lineBuffer == "static")
        {
            return Token(Token::SEG, lineBuffer);
        }
        else if (lineBuffer == "constant")
        {
            return Token(Token::SEG, lineBuffer);
        }
        else if (lineBuffer == "this")
        {
            return Token(Token::SEG, lineBuffer);
        }
        else if (lineBuffer == "that")
        {
            return Token(Token::SEG, lineBuffer);
        }
        else if (lineBuffer == "pointer")
        {
            return Token(Token::SEG, lineBuffer);
        }
        else if (lineBuffer == "temp")
        {
            return Token(Token::SEG, lineBuffer);
        }
        else if (lineBuffer == "add")
        {
            return Token(Token::ADD, lineBuffer);
        }
        else if (lineBuffer == "sub")
        {
            return Token(Token::SUB, lineBuffer);
        }
        else if (lineBuffer == "neg")
        {
            return Token(Token::NEG, lineBuffer);
        }
        else if (lineBuffer == "eq")
        {
            return Token(Token::EQ, lineBuffer);
        }
        else if (lineBuffer == "gt")
        {
            return Token(Token::GT, lineBuffer);
        }
        else if (lineBuffer == "lt")
        {
            return Token(Token::LT, lineBuffer);
        }
        else if (lineBuffer == "and")
        {
            return Token(Token::AND, lineBuffer);
        }
        else if (lineBuffer == "or")
        {
            return Token(Token::OR, lineBuffer);
        }
        else if (lineBuffer == "not")
        {
            return Token(Token::NOT, lineBuffer);
        }
    }
    if (mLineIterator == mLine.end())
    {
        return Token(Token::LEND, lineBuffer);
    }
    while (std::isdigit(*mLineIterator))
    {
        lineBuffer += *mLineIterator++;
        if (!std::isdigit(*mLineIterator))
        {
            return Token(Token::IDX, lineBuffer);
        }
    }
    return Token(Token::INV, lineBuffer);
}