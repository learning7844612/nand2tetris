#include <algorithm>
#include <code/codewriter.h>
#include <exceptions/outofmemoryexception.h>
#include <fstream>
#include <iostream>
#include <list>
#include <parser/parser.h>
#include <string>

#define STATIC_SEGMENT_MAX 239

CodeWriter::CodeWriter(std::string outFileName) : outFile(std::ofstream(outFileName))
{
    if (outFile.fail())
    {
        throw std::runtime_error("Error while opening file " + outFileName);
    }
    staticVarList = nullptr;
    eqLabelCounter = 0;
    gtLabelCounter = 0;
    ltLabelCounter = 0;
    staticVarCounter = 0;
}

CodeWriter::~CodeWriter()
{
    outFile.close();
    delete staticVarList;
}

void CodeWriter::setFileName(std::string fileName)
{
    if (staticVarList)
    {
        delete staticVarList;
    }
    staticVarList = new std::list<std::string>;
    currentFileName = fileName;
    eqLabelCounter = 0;
    gtLabelCounter = 0;
    ltLabelCounter = 0;
    outFile << "// Begin file " << fileName << ".vm" << std::endl;
}

void CodeWriter::writeArithmetic(std::string command)
{
    if (command == "add")
    {
        outFile << "// add" << std::endl
                << "@SP" << std::endl
                << "AM=M-1" << std::endl
                << "D=M" << std::endl
                << "A=A-1" << std::endl
                << "D=D+M" << std::endl
                << "M=D" << std::endl;
    }
    else if (command == "sub")
    {
        outFile << "// sub" << std::endl
                << "@SP" << std::endl
                << "AM=M-1" << std::endl
                << "D=M" << std::endl
                << "A=A-1" << std::endl
                << "D=M-D" << std::endl
                << "M=D" << std::endl;
    }
    else if (command == "neg")
    {
        outFile << "// neg" << std::endl
                << "@SP" << std::endl
                << "A=M-1" << std::endl
                << "D=M" << std::endl
                << "D=!D" << std::endl
                << "D=D+1" << std::endl
                << "M=D" << std::endl;
    }
    else if (command == "eq")
    {
        outFile << "// eq" << std::endl
                << "@SP" << std::endl
                << "AM=M-1" << std::endl
                << "D=M" << std::endl
                << "A=A-1" << std::endl
                << "D=M-D" << std::endl
                << "@" << currentFileName << ".NEQ." << std::to_string(eqLabelCounter) << std::endl
                << "D;JNE" << std::endl
                << "@SP" << std::endl
                << "A=M-1" << std::endl
                << "M=-1" << std::endl
                << "@" << currentFileName << ".EQ." << std::to_string(eqLabelCounter) << std::endl
                << "0;JMP" << std::endl
                << "(" << currentFileName << ".NEQ." << std::to_string(eqLabelCounter) << ")" << std::endl
                << "@SP" << std::endl
                << "A=M-1" << std::endl
                << "M=0" << std::endl
                << "(" << currentFileName << ".EQ." << std::to_string(eqLabelCounter) << ")" << std::endl;
        eqLabelCounter++;
    }
    else if (command == "gt")
    {
        outFile << "// gt" << std::endl
                << "@SP" << std::endl
                << "AM=M-1" << std::endl
                << "D=M" << std::endl
                << "A=A-1" << std::endl
                << "D=M-D" << std::endl
                << "@" << currentFileName << ".NGT." << std::to_string(gtLabelCounter) << std::endl
                << "D;JLE" << std::endl
                << "@SP" << std::endl
                << "A=M-1" << std::endl
                << "M=-1" << std::endl
                << "@" << currentFileName << ".GT." << std::to_string(gtLabelCounter) << std::endl
                << "0;JMP" << std::endl
                << "(" << currentFileName << ".NGT." << std::to_string(gtLabelCounter) << ")" << std::endl
                << "@SP" << std::endl
                << "A=M-1" << std::endl
                << "M=0" << std::endl
                << "(" << currentFileName << ".GT." << std::to_string(gtLabelCounter) << ")" << std::endl;
        gtLabelCounter++;
    }
    else if (command == "lt")
    {
        outFile << "// lt" << std::endl
                << "@SP" << std::endl
                << "AM=M-1" << std::endl
                << "D=M" << std::endl
                << "A=A-1" << std::endl
                << "D=M-D" << std::endl
                << "@" << currentFileName << ".NLT." << std::to_string(ltLabelCounter) << std::endl
                << "D;JGE" << std::endl
                << "@SP" << std::endl
                << "A=M-1" << std::endl
                << "M=-1" << std::endl
                << "@" << currentFileName << ".LT." << std::to_string(ltLabelCounter) << std::endl
                << "0;JMP" << std::endl
                << "(" << currentFileName << ".NLT." << std::to_string(ltLabelCounter) << ")" << std::endl
                << "@SP" << std::endl
                << "A=M-1" << std::endl
                << "M=0" << std::endl
                << "(" << currentFileName << ".LT." << std::to_string(ltLabelCounter) << ")" << std::endl;
        ltLabelCounter++;
    }
    else if (command == "and")
    {
        outFile << "// and" << std::endl
                << "@SP" << std::endl
                << "AM=M-1" << std::endl
                << "D=M" << std::endl
                << "@SP" << std::endl
                << "A=M-1" << std::endl
                << "D=D&M" << std::endl
                << "M=D" << std::endl;
    }
    else if (command == "or")
    {
        outFile << "// or" << std::endl
                << "@SP" << std::endl
                << "AM=M-1" << std::endl
                << "D=M" << std::endl
                << "@SP" << std::endl
                << "A=M-1" << std::endl
                << "D=D|M" << std::endl
                << "M=D" << std::endl;
    }
    else if (command == "not")
    {
        outFile << "// not" << std::endl
                << "@SP" << std::endl
                << "A=M-1" << std::endl
                << "D=M" << std::endl
                << "D=!D" << std::endl
                << "M=D" << std::endl;
    }
}

void CodeWriter::writePushPop(Parser::command commandType, std::string segment, int index)
{
    if (commandType == Parser::C_PUSH)
    {
        if (segment == "constant")
        {
            outFile << "// push constant " << std::to_string(index) << std::endl
                    << "@" << std::to_string(index) << std::endl
                    << "D=A" << std::endl
                    << "@SP" << std::endl
                    << "A=M" << std::endl
                    << "M=D" << std::endl
                    << "@SP" << std::endl
                    << "M=M+1" << std::endl;
        }
        else if (segment == "local")
        {
            outFile << "// push local " << std::to_string(index) << std::endl
                    << "@" << std::to_string(index) << std::endl
                    << "D=A" << std::endl
                    << "@LCL" << std::endl
                    << "A=M" << std::endl
                    << "A=D+A" << std::endl
                    << "D=M" << std::endl
                    << "@SP" << std::endl
                    << "A=M" << std::endl
                    << "M=D" << std::endl
                    << "@SP" << std::endl
                    << "M=M+1" << std::endl;
        }
        else if (segment == "argument")
        {
            outFile << "// push argument " << std::to_string(index) << std::endl
                    << "@" << std::to_string(index) << std::endl
                    << "D=A" << std::endl
                    << "@ARG" << std::endl
                    << "A=M" << std::endl
                    << "A=D+A" << std::endl
                    << "D=M" << std::endl
                    << "@SP" << std::endl
                    << "A=M" << std::endl
                    << "M=D" << std::endl
                    << "@SP" << std::endl
                    << "M=M+1" << std::endl;
        }
        else if (segment == "this")
        {
            outFile << "// push this " << std::to_string(index) << std::endl
                    << "@" << std::to_string(index) << std::endl
                    << "D=A" << std::endl
                    << "@THIS" << std::endl
                    << "A=M" << std::endl
                    << "A=D+A" << std::endl
                    << "D=M" << std::endl
                    << "@SP" << std::endl
                    << "A=M" << std::endl
                    << "M=D" << std::endl
                    << "@SP" << std::endl
                    << "M=M+1" << std::endl;
        }
        else if (segment == "that")
        {
            outFile << "// push that " << std::to_string(index) << std::endl
                    << "@" << std::to_string(index) << std::endl
                    << "D=A" << std::endl
                    << "@THAT" << std::endl
                    << "A=M" << std::endl
                    << "A=D+A" << std::endl
                    << "D=M" << std::endl
                    << "@SP" << std::endl
                    << "A=M" << std::endl
                    << "M=D" << std::endl
                    << "@SP" << std::endl
                    << "M=M+1" << std::endl;
        }
        else if (segment == "pointer")
        {
            outFile << "// push pointer " << std::to_string(index) << std::endl
                    << "@" << std::to_string(index) << std::endl
                    << "D=A" << std::endl
                    << "@3" << std::endl
                    << "A=D+A" << std::endl
                    << "D=M" << std::endl
                    << "@SP" << std::endl
                    << "A=M" << std::endl
                    << "M=D" << std::endl
                    << "@SP" << std::endl
                    << "M=M+1" << std::endl;
        }
        else if (segment == "temp")
        {
            outFile << "// push temp " << std::to_string(index) << std::endl
                    << "@" << std::to_string(index) << std::endl
                    << "D=A" << std::endl
                    << "@5" << std::endl
                    << "A=D+A" << std::endl
                    << "D=M" << std::endl
                    << "@SP" << std::endl
                    << "A=M" << std::endl
                    << "M=D" << std::endl
                    << "@SP" << std::endl
                    << "M=M+1" << std::endl;
        }
        else if (segment == "static")
        {
            if (staticVarCounter > STATIC_SEGMENT_MAX)
            {
                throw OutOfMemoryException("out of memory for static storage");
            }
            outFile << "// push static " << std::to_string(index) << std::endl
                    << "@" << currentFileName << "." << index << std::endl
                    << "D=M" << std::endl
                    << "@SP" << std::endl
                    << "A=M" << std::endl
                    << "M=D" << std::endl
                    << "@SP" << std::endl
                    << "M=M+1" << std::endl;
            std::string staticVar = currentFileName + "." + std::to_string(index);
            if (!staticVarExists(staticVar))
            {
                staticVarList->push_back(staticVar);
                staticVarCounter++;
            }
        }
    }
    else if (commandType == Parser::C_POP)
    {
        if (segment == "local")
        {
            outFile << "// pop local " << std::to_string(index) << std::endl
                    << "@" << std::to_string(index) << std::endl
                    << "D=A" << std::endl
                    << "@LCL" << std::endl
                    << "A=M" << std::endl
                    << "D=D+A" << std::endl
                    << "@R13" << std::endl
                    << "M=D" << std::endl
                    << "@SP" << std::endl
                    << "AM=M-1" << std::endl
                    << "D=M" << std::endl
                    << "@R13" << std::endl
                    << "A=M" << std::endl
                    << "M=D" << std::endl;
        }
        else if (segment == "argument")
        {
            outFile << "// pop argument " << std::to_string(index) << std::endl
                    << "@" << std::to_string(index) << std::endl
                    << "D=A" << std::endl
                    << "@ARG" << std::endl
                    << "A=M" << std::endl
                    << "D=D+A" << std::endl
                    << "@R13" << std::endl
                    << "M=D" << std::endl
                    << "@SP" << std::endl
                    << "AM=M-1" << std::endl
                    << "D=M" << std::endl
                    << "@R13" << std::endl
                    << "A=M" << std::endl
                    << "M=D" << std::endl;
        }
        else if (segment == "this")
        {
            outFile << "// pop this " << std::to_string(index) << std::endl
                    << "@" << std::to_string(index) << std::endl
                    << "D=A" << std::endl
                    << "@THIS" << std::endl
                    << "A=M" << std::endl
                    << "D=D+A" << std::endl
                    << "@R13" << std::endl
                    << "M=D" << std::endl
                    << "@SP" << std::endl
                    << "AM=M-1" << std::endl
                    << "D=M" << std::endl
                    << "@R13" << std::endl
                    << "A=M" << std::endl
                    << "M=D" << std::endl;
        }
        else if (segment == "that")
        {
            outFile << "// pop that " << std::to_string(index) << std::endl
                    << "@" << std::to_string(index) << std::endl
                    << "D=A" << std::endl
                    << "@THAT" << std::endl
                    << "A=M" << std::endl
                    << "D=D+A" << std::endl
                    << "@R13" << std::endl
                    << "M=D" << std::endl
                    << "@SP" << std::endl
                    << "AM=M-1" << std::endl
                    << "D=M" << std::endl
                    << "@R13" << std::endl
                    << "A=M" << std::endl
                    << "M=D" << std::endl;
        }
        else if (segment == "pointer")
        {
            outFile << "// pop pointer " << std::to_string(index) << std::endl
                    << "@" << std::to_string(index) << std::endl
                    << "D=A" << std::endl
                    << "@3" << std::endl
                    << "D=D+A" << std::endl
                    << "@R13" << std::endl
                    << "M=D" << std::endl
                    << "@SP" << std::endl
                    << "AM=M-1" << std::endl
                    << "D=M" << std::endl
                    << "@R13" << std::endl
                    << "A=M" << std::endl
                    << "M=D" << std::endl;
        }
        else if (segment == "temp")
        {
            outFile << "// pop temp " << std::to_string(index) << std::endl
                    << "@" << std::to_string(index) << std::endl
                    << "D=A" << std::endl
                    << "@5" << std::endl
                    << "D=D+A" << std::endl
                    << "@R13" << std::endl
                    << "M=D" << std::endl
                    << "@SP" << std::endl
                    << "AM=M-1" << std::endl
                    << "D=M" << std::endl
                    << "@R13" << std::endl
                    << "A=M" << std::endl
                    << "M=D" << std::endl;
        }
        else if (segment == "static")
        {
            if (staticVarCounter > STATIC_SEGMENT_MAX)
            {
                throw OutOfMemoryException("out of memory for static storage");
            }
            outFile << "// pop static " << std::to_string(index) << std::endl
                    << "@SP" << std::endl
                    << "AM=M-1" << std::endl
                    << "D=M" << std::endl
                    << "@" << currentFileName << "." << index << std::endl
                    << "M=D" << std::endl;
            std::string staticVar = currentFileName + "." + std::to_string(index);
            if (!staticVarExists(staticVar))
            {
                staticVarList->push_back(staticVar);
                staticVarCounter++;
            }
        }
    }
}

bool CodeWriter::staticVarExists(std::string var)
{
    auto listIterator = std::find(staticVarList->begin(), staticVarList->end(), var);
    if (listIterator != staticVarList->end())
    {
        return true;
    }
    return false;
}