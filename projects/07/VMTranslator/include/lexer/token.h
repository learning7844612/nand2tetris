#ifndef TOKEN_H
#define TOKEN_H

#include <string>

class Token
{
public:
    enum token
    {
        PUSH,
        POP,
        ADD,
        SUB,
        NEG,
        EQ,
        GT,
        LT,
        AND,
        OR,
        NOT,
        SEG,
        IDX,
        LEND,
        INV
    };
    Token();
    Token(enum token, std::string);
    token mToken;
    std::string mValue;
};

#endif /** TOKEN_H */