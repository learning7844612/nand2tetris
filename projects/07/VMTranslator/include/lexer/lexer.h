#ifndef LEXER_H
#define LEXER_H

#include <fstream>
#include <lexer/token.h>
#include <string>

class Lexer
{
public:
    Lexer();
    Lexer(std::string &);
    Token getToken();

private:
    std::string mLine;
    std::string::iterator mLineIterator;
    int mColumn;
};

#endif /** LEXER_H */