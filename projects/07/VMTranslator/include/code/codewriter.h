#ifndef CODEWRITER_H
#define CODEWRITER_H

#include <fstream>
#include <list>
#include <parser/parser.h>
#include <string>

class CodeWriter
{
public:
    CodeWriter();
    CodeWriter(std::string);
    ~CodeWriter();
    void setFileName(std::string);
    void writeArithmetic(std::string);
    void writePushPop(Parser::command, std::string, int);
    void close();

private:
    bool staticVarExists(std::string);
    std::ofstream outFile;
    std::string currentFileName;
    int eqLabelCounter;
    int gtLabelCounter;
    int ltLabelCounter;
    int staticVarCounter;
    std::list<std::string> *staticVarList;
};

#endif /** CODEWRITER_H */