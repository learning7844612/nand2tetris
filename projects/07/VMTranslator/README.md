# Hack VM Translator (Part 1)
This is the repository of my implementation of the Hack VM Translator for the project n° 7 of the Nand2Tetris book. The vm translator is implemented in c++ and uses the CMake build system. This is implementation might not be the most elegant or optimal since i'm not fluent with C++ yet. This implementation has redundant code around the lexer/parser since the goal was to have a working solution first. Better modularized code will be added later. The implementation might be overengineered but the goal was to learn how to implement an assembler and do it for the Hack platform. This is why i decided to add a tokenizer for such a simple language and do the lexycal scanning manually instead of using regular expressions or things like Flex/Bison or equivalent. The tokenizer code could be split further but i went for a working solution first. This was tested working on Linux and on Windows. It might work on any other system that has a c++ compiler and CMake but wasn't tested.
## Build instructions:
The VM Translator was developed to run on all the common operating sytems (*Linux*, *Windows*, *MacOS*). The software exploits the *CMake* build system for cross platform compilation.
### Prerequisistes:
- *CMake* on all platforms.  
- A C++ toolchain (*GCC*/*LLVM/*Clang*) on Posix systems.
- *Microsoft Visual Studio* on *Windows* systems.
### Building on POSIX systems:
Clone this repository, open a terminal emulator and cd into the **'nand2tetris/projects/07/VMTranslator'**.  
At this point you are in the same directory where the CMakeLists.txt file is located. At this point type the following commands:
```
./build.sh --setup
./build.sh --compile
```
### Additional commands on POSIX systems:
The command below cleans che CMake generated build system files and removes the bin folder and the compiled program.
```
./build.sh --clean-setup
```
The command below invokes the CMake generated Makefile by passing the clean flag.
```
./build.sh --clean-compile
```
### Building on Windows systems:
Follow the same instructions as for posix and then run the following commands:
```
.\build.bat --setup
.\build.bat --compile
```
### Additional commands on Windows systems:
The command below cleans che CMake generated build system files and removes the bin folder and the compiled program.
```
.\build.bat --clean-setup
```
## Note:
On Windows systems cmake generates a Microsoft Visual Studio solution file that can be optionally opened in the Microsoft Visual Studio IDE and dealt with from the GUI of the IDE itself.
The batch script uses the MSBuild system which can only be run from a Windows Developer Powershell. You can open the Windows Developer Powershell from within the Microsoft Visual Studi IDE or by launching it from the Windows Gui by searching for "Developer Powershell".
## Running
At this point if the compilation was successful the binary will be found in the bin directory. To assemble some hack file simply invoke the assembler:
### Posix
```
./bin/VMTranslator /path/to/file.vm
```
### Windows
```
.\bin\VMTranslator.exe \path\to\file.vm
```