#include <algorithm>
#include <code/codewriter.h>
#include <exceptions/outofmemoryexception.h>
#include <exceptions/parserexception.h>
#include <filesystem>
#include <iostream>
#include <parser/parser.h>

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        std::cout << "Usage: " << std::endl
                  << "(1) " << argv[0] << " filename.vm" << std::endl
                  << "(2) " << argv[0] << " directoryname" << std::endl;
        return -1;
    }
    std::filesystem::path path(argv[1]);
    if (path.has_extension())
    {
        if (path.extension() == ".vm")
        {
            try
            {
                Parser p(argv[1]);
                CodeWriter c(path.replace_extension(".asm").string());
                c.setFileName(path.stem().string());
                while (p.hasMoreCommands())
                {
                    p.advance();
                    switch (p.commandType())
                    {
                    case Parser::C_ARITHMETIC:
                        c.writeArithmetic(p.arg1());
                        break;
                    case Parser::C_PUSH:
                        c.writePushPop(p.commandType(), p.arg1(), p.arg2());
                        break;
                    case Parser::C_POP:
                        c.writePushPop(p.commandType(), p.arg1(), p.arg2());
                        break;
                    default:
                        continue;
                    }
                }
            }
            catch (ParserException &ex)
            {
                std::cout << "File: " << path.stem().string() << " " << ex.what() << std::endl;
                return -1;
            }
            catch (std::runtime_error &ex)
            {
                std::cout << "File: " << path.stem().string() << " " << ex.what() << std::endl;
                return -1;
            }
            catch (OutOfMemoryException &ex)
            {
                std::cout << "File: " << path.stem().string() << " " << ex.what() << std::endl;
                return -1;
            }
        }
        else
        {
            std::cout << argv[1] << " is not a .vm file" << std::endl;
            return -1;
        }
    }
    else
    {
        try
        {
            std::string outFileName;
            std::string pathSeparator(1, std::filesystem::path::preferred_separator);
            if (path.has_filename())
            {
                outFileName = path.string() + pathSeparator + path.filename().string() + ".asm";
            }
            else
            {
                outFileName = path.string() + path.parent_path().filename().string() + ".asm";
            }
            CodeWriter c(outFileName);
            for (auto &entry : std::filesystem::directory_iterator(argv[1]))
            {
                if (entry.path().has_extension())
                {
                    std::string currentFileExtension = entry.path().extension().string();
                    if (currentFileExtension == ".vm")
                    {
                        try
                        {
                            Parser p(entry.path().string());
                            c.setFileName(entry.path().stem().string());
                            while (p.hasMoreCommands())
                            {
                                p.advance();
                                switch (p.commandType())
                                {
                                case Parser::C_ARITHMETIC:
                                    c.writeArithmetic(p.arg1());
                                    break;
                                case Parser::C_PUSH:
                                    c.writePushPop(p.commandType(), p.arg1(), p.arg2());
                                    break;
                                case Parser::C_POP:
                                    c.writePushPop(p.commandType(), p.arg1(), p.arg2());
                                    break;
                                default:
                                    continue;
                                }
                            }
                        }
                        catch (ParserException &ex)
                        {
                            std::cout << "File: " << entry.path().filename().string() << " " << ex.what() << std::endl;
                            return -1;
                        }
                        catch (std::runtime_error &ex)
                        {
                            std::cout << "File: " << entry.path().filename().string() << " " << ex.what() << std::endl;
                            return -1;
                        }
                        catch (OutOfMemoryException &ex)
                        {
                            std::cout << "File: " << entry.path().filename().string() << " " << ex.what() << std::endl;
                            return -1;
                        }
                    }
                }
            }
        }
        catch (std::runtime_error &ex)
        {
            std::cout << ex.what() << std::endl;
            return -1;
        }
    }
    return 0;
}