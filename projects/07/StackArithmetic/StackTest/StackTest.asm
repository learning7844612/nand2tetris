// Begin file StackTest.vm
// push constant 17
@17
D=A
@SP
A=M
M=D
@SP
M=M+1
// push constant 17
@17
D=A
@SP
A=M
M=D
@SP
M=M+1
// eq
@SP
AM=M-1
D=M
A=A-1
D=M-D
@StackTest.NEQ.0
D;JNE
@SP
A=M-1
M=-1
@StackTest.EQ.0
0;JMP
(StackTest.NEQ.0)
@SP
A=M-1
M=0
(StackTest.EQ.0)
// push constant 17
@17
D=A
@SP
A=M
M=D
@SP
M=M+1
// push constant 16
@16
D=A
@SP
A=M
M=D
@SP
M=M+1
// eq
@SP
AM=M-1
D=M
A=A-1
D=M-D
@StackTest.NEQ.1
D;JNE
@SP
A=M-1
M=-1
@StackTest.EQ.1
0;JMP
(StackTest.NEQ.1)
@SP
A=M-1
M=0
(StackTest.EQ.1)
// push constant 16
@16
D=A
@SP
A=M
M=D
@SP
M=M+1
// push constant 17
@17
D=A
@SP
A=M
M=D
@SP
M=M+1
// eq
@SP
AM=M-1
D=M
A=A-1
D=M-D
@StackTest.NEQ.2
D;JNE
@SP
A=M-1
M=-1
@StackTest.EQ.2
0;JMP
(StackTest.NEQ.2)
@SP
A=M-1
M=0
(StackTest.EQ.2)
// push constant 892
@892
D=A
@SP
A=M
M=D
@SP
M=M+1
// push constant 891
@891
D=A
@SP
A=M
M=D
@SP
M=M+1
// lt
@SP
AM=M-1
D=M
A=A-1
D=M-D
@StackTest.NLT.0
D;JGE
@SP
A=M-1
M=-1
@StackTest.LT.0
0;JMP
(StackTest.NLT.0)
@SP
A=M-1
M=0
(StackTest.LT.0)
// push constant 891
@891
D=A
@SP
A=M
M=D
@SP
M=M+1
// push constant 892
@892
D=A
@SP
A=M
M=D
@SP
M=M+1
// lt
@SP
AM=M-1
D=M
A=A-1
D=M-D
@StackTest.NLT.1
D;JGE
@SP
A=M-1
M=-1
@StackTest.LT.1
0;JMP
(StackTest.NLT.1)
@SP
A=M-1
M=0
(StackTest.LT.1)
// push constant 891
@891
D=A
@SP
A=M
M=D
@SP
M=M+1
// push constant 891
@891
D=A
@SP
A=M
M=D
@SP
M=M+1
// lt
@SP
AM=M-1
D=M
A=A-1
D=M-D
@StackTest.NLT.2
D;JGE
@SP
A=M-1
M=-1
@StackTest.LT.2
0;JMP
(StackTest.NLT.2)
@SP
A=M-1
M=0
(StackTest.LT.2)
// push constant 32767
@32767
D=A
@SP
A=M
M=D
@SP
M=M+1
// push constant 32766
@32766
D=A
@SP
A=M
M=D
@SP
M=M+1
// gt
@SP
AM=M-1
D=M
A=A-1
D=M-D
@StackTest.NGT.0
D;JLE
@SP
A=M-1
M=-1
@StackTest.GT.0
0;JMP
(StackTest.NGT.0)
@SP
A=M-1
M=0
(StackTest.GT.0)
// push constant 32766
@32766
D=A
@SP
A=M
M=D
@SP
M=M+1
// push constant 32767
@32767
D=A
@SP
A=M
M=D
@SP
M=M+1
// gt
@SP
AM=M-1
D=M
A=A-1
D=M-D
@StackTest.NGT.1
D;JLE
@SP
A=M-1
M=-1
@StackTest.GT.1
0;JMP
(StackTest.NGT.1)
@SP
A=M-1
M=0
(StackTest.GT.1)
// push constant 32766
@32766
D=A
@SP
A=M
M=D
@SP
M=M+1
// push constant 32766
@32766
D=A
@SP
A=M
M=D
@SP
M=M+1
// gt
@SP
AM=M-1
D=M
A=A-1
D=M-D
@StackTest.NGT.2
D;JLE
@SP
A=M-1
M=-1
@StackTest.GT.2
0;JMP
(StackTest.NGT.2)
@SP
A=M-1
M=0
(StackTest.GT.2)
// push constant 57
@57
D=A
@SP
A=M
M=D
@SP
M=M+1
// push constant 31
@31
D=A
@SP
A=M
M=D
@SP
M=M+1
// push constant 53
@53
D=A
@SP
A=M
M=D
@SP
M=M+1
// add
@SP
AM=M-1
D=M
A=A-1
D=D+M
M=D
// push constant 112
@112
D=A
@SP
A=M
M=D
@SP
M=M+1
// sub
@SP
AM=M-1
D=M
A=A-1
D=M-D
M=D
// neg
@SP
A=M-1
D=M
D=!D
D=D+1
M=D
// or
@SP
AM=M-1
D=M
@SP
A=M-1
D=D&M
M=D
// push constant 82
@82
D=A
@SP
A=M
M=D
@SP
M=M+1
// or
@SP
AM=M-1
D=M
@SP
A=M-1
D=D|M
M=D
// not
@SP
A=M-1
D=M
D=!D
M=D
