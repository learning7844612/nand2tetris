# From Nand to Tetris
This repository contains the author's projects related to the courses found at http://www.nand2tetris.org/ and the book "The Elements of Computing Systems" by Noam Nisan and Shimon Schocken.
## Hardware (chapters 1 - 5):
The book's hardware projects were done with the least possible gates usage wherever possible.
## Software (chapters 6 - 13):
The book's sofware projects were implemented with a best effort approach, that means the code was not optimized for speed or memory consumption but for achieving the working solution as fast as possible.
The projects were implemented by using C++ and the CMake build system so they can be cross compiled on a variety of operating systems where a C++ compiler and CMake are available. For sure the mainstream ones (Linux, Mac os, Windows). The author is not very much fluent with C++ yet (which he's learning) so the code might be ugly and not respecting the best practices for C++. Be warned that there might be code redundancies in the code of the various projects that will be better refactored in a future update.